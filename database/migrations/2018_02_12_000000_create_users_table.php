<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32)->nullable();
            $table->string('email', 32)->unique();
            $table->string('password', 100);
            $table->string('verifyToken',25)->nullable();
            $table->boolean('status')->default(0);
            $table->string('no_telp', 12)->nullable();
            $table->string('jk', 1)->nullable();
            $table->text('alamat')->nullable();
            $table->binary('foto_profil')->nullable();
            $table->string('remember_token', 100)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *s
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
