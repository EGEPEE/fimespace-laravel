<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoworkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coworkings', function (Blueprint $table) {
            $table->increments('id_co');
            $table->integer('id_user')->unsigned()->index();
            $table->string('nama_co', 32);
            $table->text('desc');
            $table->string('nama_utama', 32);
            $table->string('bank_utama', 10);
            $table->string('rek_utama', 16);
            $table->string('nama_kedua', 32)->nullable();
            $table->string('bank_kedua', 10)->nullable();
            $table->string('rek_kedua', 16)->nullable();
            $table->string('kota', 255);
            $table->text('alamat_co');
            $table->float('lat', 9, 6);
            $table->float('lng', 9, 6);
            $table->string('email_co', 32);
            $table->string('telp', 12);
            $table->string('website', 100)->nullable();
            $table->binary('logo_co')->nullable();
            $table->binary('header_co')->nullable();
            $table->string('fb', 100)->nullable();
            $table->string('twitter', 100)->nullable();
            $table->string('ig', 100)->nullable();
            $table->text('fasilitas');
            $table->text('coworking_desc')->nullable();
            $table->integer('coworking_perhari')->unsigned()->nullable();
            $table->integer('coworking_perbulan')->unsigned()->nullable();
            $table->integer('coworking_pertahun')->unsigned()->nullable();
            $table->text('meeting_desc')->nullable();
            $table->integer('meeting_perjam')->unsigned()->nullable();
            $table->integer('meeting_perhari')->unsigned()->nullable();
            $table->integer('meeting_perbulan')->unsigned()->nullable();
            $table->text('event_desc')->nullable();
            $table->integer('event_harga')->unsigned()->nullable();
            // $table->binary('harga_gambar')->nullable();
            $table->string('bayar_ots', 5);
            $table->string('persetujuan', 6);

            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coworkings');
        // $table->dropForeign('coworkings_id_user_foreign');
        // $table->dropIndex('coworkings_id_user_index');
        // $table->dropColumn('id_user');
    }
}
