<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCophotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cophoto', function (Blueprint $table) {
            $table->increments('id_foto');
            $table->integer('id_co')->unsigned()->index();
            $table->binary('foto_co')->nullable();

            $table->foreign('id_co')->references('id_co')->on('coworkings')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cophoto');
    }
}
