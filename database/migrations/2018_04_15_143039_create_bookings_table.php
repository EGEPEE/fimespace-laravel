<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id_booking');
            $table->integer('id_user')->unsigned()->index();
            $table->integer('id_co')->unsigned();
            $table->string('nama', 32);
            $table->string('email', 32);
            $table->string('telp', 12);
            $table->string('jenis_ruang', 12);
            $table->string('jenis_waktu', 10);
            $table->string('waktu_sewa', 2);
            $table->integer('banyak_orang')->unsigned();
            $table->date('tgl_sewa');
            $table->string('total_bayar', 10);
            $table->string('status_bayar', 10);
            $table->string('status_booking', 10);
            $table->text('pesan')->nullable();
            $table->text('testimoni')->nullable();
            $table->date('tgl_testimoni')->nullable();

            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_co')->references('id_co')->on('coworkings')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('bookings');
        // $table->dropForeign('booking_id_user_foreign');
        // $table->dropForeign('booking_id_co_foreign');
        // $table->dropIndex('booking_id_user_index');
        // $table->dropIndex('booking_id_co_index');
        // $table->dropColumn('id_user');
        // $table->dropColumn('id_co');
    }
}
