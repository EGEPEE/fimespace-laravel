@extends('layouts.master')
@section('title')
	Beranda
@endsection

@section('header')
    <!-- THEME STYLES -->
    <link href="{{ asset('css/timeline.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/light-box.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/templatemo-main.css') }}" rel="stylesheet" type="text/css"/>

    <script src="{{ asset('js/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
@endsection

@section('content')
      <nav>
        <div class="logo">
            <img src="{{ asset('assets/logo/logo-blue.png')}}" alt="Logo Fimespace">
        </div>
        <div class="mini-logo">
                <img src="{{ asset('assets/logo/mini-logo.png')}}" alt="Logo Fimespace">
        </div>
        <ul>
            <li><a href="#Beranda"><i class="fa fa-home fa-2x"></i> <em>Beranda</em></a></li>
            <li><a href="#Apa-Itu-Fimespace"><i class="fa fa-heart fa-2x"></i> <em>Apa itu Fimespace?</em></a></li>
            <li><a href="#List-Coworking"><i class="fa fa-building fa-2x"></i> <em>List Coworking</em></a></li>
            <li><a href="#Tentang-Coworkers"><i class="fa fa-user fa-2x"></i> <em>Tentang Coworkers</em></a></li>
            <li><a href="#Tentang-Coworking"><i class="fa fa-list-ol fa-2x"></i> <em>Tentang Coworking</em></a></li>
        </ul>
      </nav>
      
      <div class="slides">
        <div class="slide" id="Beranda">
            <div class="content first-content">
            <div class="container-fluid">
                <div class="col-md-3">
                </div>
                <div class="col-md-9">
                    <h2>Selamat Datang di FimeSpace!</h2>
                    <p>Pilih <em>Coworking Spacemu</em> disini!</p>
                    @guest
                        <div class="main-btn"><a href="{{ url('login') }}">Masuk</a></div>
                        <div class="fb-btn"><a href="{{ url('register') }}">Registerasi</a></div>
                    @else
                        <div class="main-btn"><a href="{{ url('dashboard') }}">Dashboard</a></div>
                        <div class="fb-btn">
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                        </div>                        
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                </div>
            </div>
            </div>
        </div>
        <div class="slide" id="Apa-Itu-Fimespace">
          <div class="content first-content">
            <div class="container-fluid">
                <div class="col-md-3">
                </div>
                <div class="col-md-9">
                    <h2>APA ITU FIMESPACE?</h2>
                    <p><em>Fime-Space atau Find and Make Coworking Space</em> merupakan website yang dibentuk untuk mencari Coworking-Space yang dibutuhkan dengan suasana yang anda sukai untuk menyelesaikan tugas, membuat event seperti seminar maupun workshop. Selain itu, jika ada yang memiliki tempat ruang kosong dan bersedia untuk dijadikan coworking, dapat diperkenalkan disini.</p>
                </div>
            </div>
          </div>
        </div>
        
        <div class="slide" id="List-Coworking">
            <div class="content fourth-content">
                <div class="container-fluid">
                    <div class="first-section">
                        <div class="container-fluid">
                            <div class="left-content">
                                <h2>List Coworking Space</h2>
                                @if(count($coworkinglist)>=1)
                                    <p>Dibawah ini merupakan coworking space yang ada di FimeSpace. Ingin booking dan liat review-nya? Silahkan daftar akun-mu untuk melihat coworking yang kamu inginkan.</p>
                                @else
                                    <p>Belum Ada Coworking Yang Bergabung</p>
                                    <div class="main-btn"><a href="{{ url('coworking/profile/create') }}">Buat Coworking Space?</a></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(count($coworkinglist)>=1)
                        <div class="row">
                            <!-- list coworking -->
                            @foreach ($coworkinglist as $coworking)
                                <div class="col-md-4 col-sm-6">
                                    <div class="item">
                                        <div class="thumb">
                                                <a href="{{ url('coworking/profile/'.$coworking->id_co) }}" data-lightbox="image-1"><div class="hover-effect">
                                                <div class="hover-content">
                                                    <h2>{{ $coworking->nama_co }}</h2>
                                                    <p><span><i class="fa fa-map-marker"></i></span> {{ $coworking->kota }}</p>
                                                </div>
                                            </div></a>
                                            <div class="image">
                                                <img src="/storage/assets/{{ $coworking->id_user }}/{{ $coworking->nama_co }}/logo_coworking/{{ $coworking->logo_co }}" alt="Logo of {{ $coworking->nama_co }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="slide" id="Tentang-Coworkers">
            <div class="content third-content">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="first-section">
                                <div class="container-fluid">
                                    <div class="left-content marginBottom">
                                        <h2>Tentang Coworkers</h2>
                                        <p>Coworkers atau teman kerja merupakan seseorang yang tergabung dalam ruang lingkup yang sama dengan ruang lingkup yang profesional. Jika anda ingin menjadi sebagai coworkers, dapat bergabung dalam suatu coworking space yang tentu nyaman untuk anda bekerja. Maka dari itu, gabung bersama Fimespace untuk mencari dan bookinglah coworking space pilihan mu.</p>
                                    </div>
                                    <hr>
                                    <div class="left-content marginBottom">
                                        <section id="services">
                                            <div class="container">
                                                <div class="row text-center">
                                                <div class="col-md-4">
                                                    <span class="fa-stack fa-4x">
                                                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                    <i class="fa fa-get-pocket fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    <h4 class="service-heading text-white">Buat Akun Pribadi</h4>
                                                    <p class="text-white">Buat akun di <em>Fimespace</em> untuk mengakses fitur-fitur yang tersedia</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <span class="fa-stack fa-4x">
                                                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                    <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    <h4 class="service-heading text-white">Lihat Coworking</h4>
                                                    <p class="text-white">Lihat detail dari coworking space yang sudah terdaftar dalam <em>Fimespace</em></p>
                                                </div>
                                                <div class="col-md-4">
                                                    <span class="fa-stack fa-4x">
                                                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                    <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    <h4 class="service-heading text-white">Booking</h4>
                                                    <p class="text-white">Pilih ruang yang tersedia oleh coworking space lalu booking tempat untuk menjadi bagian dari coworkers.</p>
                                                </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <hr>
                                    <div class="left-content marginBottom">
                                        <h2>Detail</h2>
                                        <p>Untuk menjadi coworkers pada suatu coworking space melalui <em>Fimespace</em>, ada beberapa tahapan, yaitu.</p>
                                        <ol type="1">
                                            <li>
                                                <h5>Buat Akun Pribadi</h5>
                                                <p>Buat akun pribadimu sendiri untuk mengakses fitur-fitur yang disediakan oleh <em>Fimespace</em>, seperti dashboard, lihat detail dari coworking space dan booking tempat di coworking space pilihanmu. Akun pribadi sangat penting dalam pengaksesan jika telah bergabung dalam <em>Fimespace</em></p>
                                            </li>
                                            <li>
                                                <h5>Lihat Coworking</h5>
                                                <p>Coworking space merupakan hal yang penting jika ingin menjadi coworkers dalam lingkup yang baik. Dengan adanya kenyamanan dalam coworking space yang mendukung pekerjaan, maka pekerjaan tersebut akan cepat selesai dengan baik. Selain itu, coworking space akan membantu dalam mendapatkan koneksi baru dengan coworkers lainnya. Coworkers tersebut dapat dari berbagai kalangan yang tentu akan bertukar pikiran, ide dan informasi. Informasi yang didapatkan jika telah membuka suatu coworking space, yaitu harga, lokasi di peta, alamat, komunikasi detail, harga sewa tenpat, dan lain-lainnya.</p>
                                            </li>
                                            <li>
                                                <h5>Booking</h5>
                                                <p>Fitur booking ini membantu calon coworkers untuk mendaftarkan diri dalam suatu coworking space. Dalam booking ini, dapat memilih ruangan yang telahdisediakan oleh masing-masing coworking dan dengan harga yang menyesuaikan juga. Selain itu, coworkers juga dapat melihat daftar list coworking space yang pernah dikunjungi.</p>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="slide" id="Tentang-Coworking">
            <div class="content third-content text-white">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="first-section">
                                <div class="container-fluid">
                                    <div class="left-content marginBottom">
                                        <h2>Tentang Coworking Space</h2>
                                        <p>Coworking Space merupakan suatu ruang perkantoran tempat pekerja mandiri atau freelancer seperti programmer lepas, desainer, dan entrepreneur saling berbagi. Coworking Space akan menyediakan ruang konferensi, meja, bangku, koneksi internet, dan lain-lainnya yang mendukung penggunanya bekerja. Tujuan utamanya adalah sebagai wadah tempat komunitas yang sinergis dan profesional bagi para penggunanya untuk mengembangkan ide-ide baru dan jejaring sosial.</p>
                                    </div>
                                    <hr>
                                    <div class="marginBottom">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <ul class="timeline">
                                                    <li>
                                                        <div class="timeline-image">
                                                        <img class="rounded-circle img-fluid" src="{{ asset('assets/3.jpg') }}" alt="">
                                                        </div>
                                                        <div class="timeline-panel">
                                                        <div class="timeline-heading">
                                                            <h4>Tahap I</h4>
                                                            <h4 class="subheading">Buat Coworking Space</h4>
                                                        </div>
                                                        <div class="timeline-body">
                                                            <p>
                                                                Buat coworking space-mu dengan mendeskripsikan mulai dari suasana tempat, lokasi dan fasilitas-fasilitas yang disediakan, tambahkan foto-foto agar calon coworkers dapat merasakan suasana yang terdapat dalam coworking spacemu.</p>
                                                        </div>
                                                        </div>
                                                    </li>
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-image">
                                                        <img class="rounded-circle img-fluid" src="{{ asset('assets/2.jpg') }}" alt="">
                                                        </div>
                                                        <div class="timeline-panel">
                                                        <div class="timeline-heading">
                                                            <h4>Tahap II</h4>
                                                            <h4 class="subheading">Informasi Jalur komunikasi</h4>
                                                        </div>
                                                        <div class="timeline-body">
                                                            <p>Sediakan informasi mengenai kontak dari coworking spacemu seperti email dan nomor telepon agar coworkers dapat mengirimkan pesan, berdiskusi one on one dengan pihak coworking space.</p>
                                                        </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="timeline-image">
                                                        <img class="rounded-circle img-fluid" src="{{ asset('assets/4.jpg') }}" alt="">
                                                        </div>
                                                        <div class="timeline-panel">
                                                        <div class="timeline-heading">
                                                            <h4>Tahap III</h4>
                                                            <h4 class="subheading">Jadwal Booking Dan Pembayaran</h4>
                                                        </div>
                                                        <div class="timeline-body">
                                                            <p>
                                                                Setelah coworkers setuju, maka coworkers akan menentukan jadwal dan lamanya pembookingan lalu jangan lupa coworking space untuk menginformasikan bagaimana pemabayaran yang disediakan, menggunakan perantara seperti bank atau dapat membayar langsung di coworking spacenya.</p>
                                                        </div>
                                                        </div>
                                                    </li>
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-image">
                                                        <h4>Gabung
                                                            <br>Coworking Space!</h4>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('footer')
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>      
    <script src="{{ asset('js/agency.min.js') }}"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
@endsection