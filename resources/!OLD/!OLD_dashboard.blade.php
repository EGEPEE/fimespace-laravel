@extends('layouts.master')

@section('title')
	Dashboard
@endsection

@section('header')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
@endsection

@section('content')
    @include('partials.navbar.navbar')
    <!-- Header -->
    <header id="search-header" class="header-wrapper home-parallax home-fade random-header" style="background-image: url( {{ asset('assets/bg-dashboard.jpg')}});">
        <div class="header-overlay"></div>
        <div class="header-wrapper-inner">
            <div class="container">
                <div class="welcome-speech">
                    <h5>Cari Coworking</h5>
                    <form action="{{ url('dashboard/search/')}}" method="GET" class="container">
                        @csrf
                        <div class="form-group row">
                            <input id="search_co" type="text" class="form-control col-10 col-sm-11" name="search_co" placeholder="Masukkan Nama Coworking, Alamat atau Kota..." required>
                            <button type="submit" class="btn btn-primary col-2  col-sm-1" role="button"><span><i class="fa fa-search"></i></span></button>
                        </div>
                    </form> 
                    @if($search == null)
                        <p></p>
                    @else
                        <h4>Hasil Pencarian: <b class="text-uppercase">{{ $search }}</b></h4>
                        <a class="btn btn-primary btn-default" role="button" href="{{ url('dashboard') }}"><b>Tampilkan Coworking</b></a>
                    @endif
                    @include('partials.messages')
                </div>
            </div>
        </div>
    </header>

    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                    </div>
                </div>
            </div>
            <div class="row row-0-gutter container">
                @if(count($datas) >=1)
                    @foreach($datas as $data)
                        <!-- start portfolio item -->
                        <div class="col-md-4 col-0-gutter dash-col">
                            <div class="ot-portfolio-item">
                                <figure class="effect-bubba">
                                    <img class="logo_dashboard" src="/storage/assets/{{ $data->id_user }}/{{ $data->nama_co }}/logo_coworking/{{ $data->logo_co }}" alt="Photo of {{ $data->nama_co }}"/>
                                    <figcaption>
                                        <h2>{{ $data->nama_co }}</h2>
                                        <p>{{ $data->kota }}</p>
                                        <a  href="#" data-toggle="modal" data-target="#modal{{$data->id_co}}" class="btn btn-primary-second text-uppercase" role="button">Selengkapnya</a>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <!-- end portfolio item -->
                    @endforeach
                    {{ $datas->links() }}
                @else
                    <div class="col-lg-12">
                        <div class="section-title text-center">
                            <h2>Tidak Ada Coworking Space Yang Terdaftar</h2>
                        </div>
                    </div>
                @endif
            </div>
        </div><!-- end container -->
    </section>

    <!-- Google Maps -->
    <div>
        <div id="map-canvas" class="height-400">
            <div class="text-center">
                <h2>Cek koneksi atau refresh page agar dapat menampilkan maps</h2>
            </div>
        </div>
    </div>

    @foreach($datas as $data)
        <div class="modal fade" id="modal{{$data->id_co}}" tabindex="-1" role="dialog" aria-labelledby="modal{{$data->id_co}}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Modal-label-1">{{ $data->nama_co }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                    </div>
                    <div class="modal-body">
                        <img src="/storage/assets/{{ $data->id_user }}/{{$data->nama_co}}/logo_coworking/{{$data->logo_co}}" alt="Photo of {{ $data->nama_co }}" class="logo_dashboard" />
                        <div class="modal-works"><span><i class="fa fa-map-marker"></i> {{ $data->kota }}</span></div>
                        <p>{{ $data->desc }}</p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ url('coworking/profile/'.$data->id_co) }}" class="btn btn-primary" role="button">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('footer')
    <script>   
        var map, myLatLang, marker;
        geoLocationInit();

        function geoLocationInit(){
            if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(success, fail);
            }else{
                alert("Browser tidak mendukung.");
            }
        }

        function success(position){
            console.log(position);
            var latval = position.coords.latitude;
            var lngval = position.coords.longitude;
            myLatLang = new google.maps.LatLng(latval, lngval);
            $('#lat').val(latval);
            $('#lng').val(lngval);

            createMap(myLatLang);
        }

        function fail(){
            alert("Silahkan cek jaringan anda dan refresh halaman kembali.");
            myLatLang = new google.maps.LatLng(-6.175392, 106.827153);
            createMap(myLatLang);
        }

        function createMap(myLatLang){
            var coworking = @json($datas);
            var data = coworking.data;
            console.log(data);
            
            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 15,
                center: myLatLang
            });
            
            for(i = 0 ; i <= data.length ; i ++) {      
                console.log("id: " + data[i].id_co + ", Nama: " + data[i].nama_co + ", lat: " + data[i].lat + ", lng: " + data[i].lng);

                latlngset = new google.maps.LatLng(data[i].lat, data[i].lng);

                var marker = new google.maps.Marker({  
                    map: map, title: 'coworking' , position: latlngset  
                });
                // map.setCenter(marker.getPosition())

                var content = '<a href="coworking/profile/' + data[i].id_co + ' "><h6>' + data[i].nama_co + '</h6></a><hr>' +
                '<p>' + data[i].alamat_co + '</p>';

                var infowindow = new google.maps.InfoWindow()

                google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map,marker);
                    };
                })(marker,content,infowindow)); 
            }
        }
    </script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCyAGNSrdEm76jTm6T-BVmGcsfBgIhJRq4&libraries=places&callback=geoLocationInit"></script>
 
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('jquery/jquery1.11.0-ui.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('js/agency.min.js') }}"></script>
@endsection