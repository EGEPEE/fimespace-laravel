@extends('layouts.app')

@section('title')
    Tambah Coworking Space
@endsection

@section('content')
    @php
    //banner variable
    $img = 'url(../../assets/bg-regist.jpg)';
    $title = '';
    $caption = 'Buat Coworking Space';
    @endphp

    <!-- Navbar -->
    @include('partials.navbar.navbar2')
    
    <!-- Header -->
    <!-- Header -->
    <header id="header" class="header-wrapper home-parallax home-fade" style="background-image: {{ $img }}">
        <div class="header-overlay"></div>
        <div class="header-wrapper-inner">
            <div class="welcome-speech">
                <h1>Buat Coworking Space-mu</h1>
                <p class="text-uppercase">list data coworking space</p>
            </div>
            <div class="container">
                <div class="addco">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                @include('partials.messages')
                                <form action="{{ url('coworking')}}" method="POST" enctype="multipart/form-data" file="true">
                                @csrf
                                    <!-- Informasi Coworking -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Informasi
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="panel-body container">
                                                <div class="form-group row">
                                                    <label for="nama_coworking" class="col-md-4 col-form-label text-md-right">Nama Coworking</label>
                                                    <div class="col-md-7">
                                                        <input id="nama_coworking" type="text" class="form-control{{ $errors->has('nama_coworking') ? ' is-invalid' : '' }}" name="nama_coworking" placeholder="Nama Coworking Space" value="{{ old('nama_coworking') }}" required>
                                                        @if ($errors->has('nama_coworking'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('nama_coworking') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="desc_coworking" class="col-md-4 col-form-label text-md-right">Deskripsi Coworking</label>
                                                    <div class="col-md-7">
                                                        <textarea class="form-control{{ $errors->has('desc_coworking') ? ' is-invalid' : '' }}" rows="5" name="desc_coworking" placeholder="deskripsikan coworking min.255 kata" required>{{ old('desc_coworking') }}</textarea>
                                                        @if ($errors->has('desc_coworking'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('desc_coworking') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="website_coworking" class="col-md-4 col-form-label text-md-right">Website URL</label>
                                                    <div class="col-md-7">
                                                        <input id="website_coworking" type="text" class="form-control{{ $errors->has('website_coworking') ? ' is-invalid' : '' }}" name="website_coworking" placeholder="Contoh: http://fimespace.com" value="{{ old('website_coworking') }}" required>
                                                        @if ($errors->has('website_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('website_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary btn-daftar" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Next</a>
                                            </div>
                                        </div>
                                    </div>
            
                                    <!-- Komunikasi Coworking -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                    Kontak Detail
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                            <div class="panel-body container">
                                                <div class="form-group row">
                                                    <label for="email_coworking" class="col-md-4 col-form-label text-md-right">Email Utama</label>
                                                    <div class="col-md-7">
                                                        <input id="email_coworking" type="email" class="form-control{{ $errors->has('email_coworking') ? ' is-invalid' : '' }}" name="email_coworking" placeholder="Contoh: fimespace@gmail.com" value="{{ old('email_coworking') }}" required>
                                                        @if ($errors->has('email_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('email_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="telp_coworking" class="col-md-4 col-form-label text-md-right">Telpon</label>
                                                    <div class="col-md-7">
                                                        <input id="telp_coworking" type="number" class="form-control{{ $errors->has('telp_coworking') ? ' is-invalid' : '' }}" name="telp_coworking" value="{{ old('telp_coworking') }}" placeholder="Contoh: 0218123456"  required>
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('telp_coworking') }}</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group row">
                                                    <label for="fb_coworking" class="col-md-4 col-form-label text-md-right">Facebook Page</label>
                                                    <div class="col-md-7">
                                                        <input id="fb_coworking" type="text" class="form-control{{ $errors->has('fb_coworking') ? ' is-invalid' : '' }}" name="fb_coworking" placeholder="Contoh: https://www.facebook.com/fimespace/" value="{{ old('fb_coworking') }}" required>
                                                        @if ($errors->has('fb_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('fb_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="twitter_coworking" class="col-md-4 col-form-label text-md-right">Twitter</label>
                                                    <div class="col-md-7">
                                                        <input id="twitter_coworking" type="text" class="form-control{{ $errors->has('twitter_coworking') ? ' is-invalid' : '' }}" name="twitter_coworking" placeholder="Contoh: https://www.twitter.com/fimespace/" value="{{ old('twitter_coworking') }}" required>
                                                        @if ($errors->has('twitter_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('twitter_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="ig_coworking" class="col-md-4 col-form-label text-md-right">Instagram</label>
                                                    <div class="col-md-7">
                                                        <input id="ig_coworking" type="text" class="form-control{{ $errors->has('ig_coworking') ? ' is-invalid' : '' }}" name="ig_coworking" placeholder="Contoh: https://www.instagram.com/fimespace/" value="{{ old('ig_coworking') }}" required>
                                                        @if ($errors->has('ig_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('ig_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary btn-daftar" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Next</a>
                                            </div>
                                        </div>
                                    </div>
            
                                    <!-- Lokasi Coworking -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingThree">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                                    Lokasi
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                            <div class="panel-body container">
                                                <div class="form-group row">
                                                    <label for="kota_coworking" class="col-md-4 col-form-label text-md-right">Kota</label>
                                                    <div class="col-md-7">
                                                        <input id="kota_coworking" type="text" class="form-control{{ $errors->has('kota_coworking') ? ' is-invalid' : '' }}" name="kota_coworking" value="{{ old('kota_coworking') }}" placeholder="Cari kota..." required>
                                                        @if ($errors->has('kota_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('kota_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="alamat_coworking" class="col-md-4 col-form-label text-md-right">Cari lokasi</label>
                                                    <div class="col-md-7">
                                                        <input id="searchmap" type="text" class="form-control{{ $errors->has('alamat_coworking') ? ' is-invalid' : '' }}" name="alamat_coworking" value="{{ old('alamat_coworking') }}" placeholder="Cari Lokasi.." required>
                                                        @if ($errors->has('alamat_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('alamat_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                        <div id="map-canvas">map</div>
                                                    </div>
                                                </div>
                                                <div class="form-group row hiddenForm">
                                                    <label for="alamat_coworking" class="col-md-4 col-form-label text-md-right"></label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input id="lng" type="text" class="form-control{{ $errors->has('lng') ? ' is-invalid' : '' }}" name="lng" value="{{ old('lng') }}" required>
                                                            @if ($errors->has('lng'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('lng') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                
                                                        <div class="col-md-6">
                                                            <input id="lat" type="text"  class="form-control{{ $errors->has('lat') ? ' is-invalid' : '' }}" name="lat" value="{{ old('lat') }}"  required>
                                                            @if ($errors->has('lat'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('lat') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary btn-daftar" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">Next</a>
                                            </div>
                                        </div>
                                    </div>
            
                                    <!-- Fasilitas Coworking -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                                    Fasilitas
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                                            <div class="panel-body container">
                                                <div class="form-group row">
                                                    <label for="fasilitas_coworking" class="col-md-4 col-form-label text-md-right">Fasilitas</label>
                                                    <div class="col-md-7">
                                                        <textarea class="form-control{{ $errors->has('fasilitas_coworking') ? ' is-invalid' : '' }}" rows="5" id="article-ckeditor" name="fasilitas_coworking" required>{{ old('fasilitas_coworking') }}</textarea>
                                                        @if ($errors->has('fasilitas_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('fasilitas_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <a  class="btn btn-primary btn-daftar" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">Next</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Foto Coworking -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingFour">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                                    Foto
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFive">
                                            <div class="panel-body container">
                                                <div class="form-group row">
                                                    <label for="logo_coworking" class="col-md-4 col-form-label text-md-right">Upload Logo</label>
                                                    <div class="col-md-7">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="customFile" value="{{ old('logo_coworking') }}" name="logo_coworking">
                                                            <label class="custom-file-label" for="customFile">{{ old('logo_coworking') }}</label>
                                                            @if ($errors->has('logo_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('logo_coworking') }}</strong>
                                                                </span>
                                                            @endif  
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="header_coworking" class="col-md-4 col-form-label text-md-right">Upload Header</label>
                                                    <div class="col-md-7">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="customFile" value="{{ old('header_coworking') }}" name="header_coworking">
                                                            <label class="custom-file-label" for="customFile"{{ old('logo_coworking') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="image_coworking" class="col-md-4 col-form-label text-md-right">Upload Gambar</label>
                                                    <div class="col-md-7">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="customFile"  name="gambar_coworking[]" multiple="multiple">
                                                            <label class="custom-file-label" for="customFile">{{ old('harga_perbulan') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a  class="btn btn-primary btn-daftar" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">Next</a>                                                
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Pembayaran Coworking -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingSix">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                                    Pembayaran Detail
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseSix" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSix">
                                            <div class="panel-body container">
                                                <div class="form-group row">
                                                    <label for="bank_utama_coworking" class="col-md-4 col-form-label text-md-right">Bank Utama</label>
                                                    <div class="col-md-7">
                                                        <input id="bank_utama_coworking" type="text" class="form-control{{ $errors->has('bank_utama_coworking') ? ' is-invalid' : '' }}" name="bank_utama_coworking" placeholder="Contoh: Bca, Mandiri, dll" value="{{ old('bank_utama_coworking') }}" required>
                                                        @if ($errors->has('bank_utama_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('bank_utama_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="rek_utama_coworking" class="col-md-4 col-form-label text-md-right">Nomor Rekening Utama</label>
                                                    <div class="col-md-7">
                                                        <input id="rek_utama_coworking" type="number" class="form-control{{ $errors->has('rek_utama_coworking') ? ' is-invalid' : '' }}" name="rek_utama_coworking" placeholder="Contoh: Bca, Mandiri, dll" value="{{ old('rek_utama_coworking') }}" required>
                                                        @if ($errors->has('rek_utama_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('rek_utama_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group row">
                                                    <label for="bank_kedua_coworking" class="col-md-4 col-form-label text-md-right">Bank Kedua</label>
                                                    <div class="col-md-7">
                                                        <input id="bank_kedua_coworking" type="text" class="form-control{{ $errors->has('bank_kedua_coworking') ? ' is-invalid' : '' }}" name="bank_kedua_coworking" placeholder="Contoh: Bca, Mandiri, dll" value="{{ old('bank_kedua_coworking') }}">
                                                        @if ($errors->has('bank_kedua_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('bank_kedua_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="rek_kedua_coworking" class="col-md-4 col-form-label text-md-right">Nomor Rekening Kedua</label>
                                                    <div class="col-md-7">
                                                        <input id="rek_kedua_coworking" type="number" class="form-control{{ $errors->has('rek_kedua_coworking') ? ' is-invalid' : '' }}" name="rek_kedua_coworking" placeholder="Contoh: Bca, Mandiri, dll" value="{{ old('rek_kedua_coworking') }}">
                                                        @if ($errors->has('rek_kedua_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('rek_kedua_coworking') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary btn-daftar" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSix">Next</a>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Harga Coworking -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingSeven">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                                    Harga
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSeven">
                                            <div class="panel-body container">
                                                <p>Tulisakan Deskripsi dan Harga dari Jenis-Jenis Coworking Anda</p>
                                                <hr>
                                                <!-- Coworking Room -->
                                                <p class="text-center">Ruang Coworking</p>
                                                <div class="form-group row">
                                                    <label for="harga_co_desc" class="col-md-4 col-form-label text-md-right">Catatan Ruang Coworking</label>
                                                    <div class="col-md-7">                                                       
                                                        <textarea class="form-control{{ $errors->has('harga_co_desc') ? ' is-invalid' : '' }}" rows="5" name="harga_co_desc" required>{{ old('harga_co_desc') }}</textarea>
                                                        @if ($errors->has('harga_co_desc'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('harga_co_desc') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="harga_perhari" class="col-md-4 col-form-label text-md-right">Harga Coworking Perhari</label>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <label for="rupiah" class="col-md-2 col-form-label">Rp.</label>
                                                            <input id="harga_perhari" type="number" class="form-control{{ $errors->has('harga_perhari') ? ' is-invalid' : '' }} col-sm-9" name="harga_perhari" placeholder="Contoh: 100.000" value="{{ old('harga_perhari') }}" required>
                                                            @if ($errors->has('harga_perhari'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_perhari') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="harga_perbulan" class="col-md-4 col-form-label text-md-right">Harga Coworking Perbulan</label>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                            <input id="harga_perbulan" type="number" class="form-control{{ $errors->has('harga_perbulan') ? ' is-invalid' : '' }} col-sm-9" name="harga_perbulan" placeholder="Contoh: 100.000" value="{{ old('harga_perbulan') }}" required>
                                                            @if ($errors->has('harga_perbulan'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_perbulan') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="harga_pertahun" class="col-md-4 col-form-label text-md-right">Harga Coworking Pertahun</label>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                            <input id="harga_pertahun" type="number" class="form-control{{ $errors->has('harga_pertahun') ? ' is-invalid' : '' }} col-sm-9" name="harga_pertahun" placeholder="Contoh: 100.000" value="{{ old('harga_pertahun') }}" required>
                                                            @if ($errors->has('harga_pertahun'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_pertahun') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <p class="text-center">Ruang Meeting atau Diskusi</p>
                                                <!-- Meeting Room -->
                                                <div class="form-group row">
                                                    <label for="harga_meeting_desc" class="col-md-4 col-form-label text-md-right">Catatan Ruang Meeting</label>
                                                    <div class="col-md-7">                                                                                                       
                                                        <textarea class="form-control{{ $errors->has('harga_meeting_desc') ? ' is-invalid' : '' }}" rows="5" name="harga_meeting_desc" value="{{ old('harga_meeting_desc') }}" required>{{ old('harga_meeting_desc') }}</textarea>
                                                        @if ($errors->has('harga_meeting_desc'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('harga_meeting_desc') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="harga_meeting_perjam" class="col-md-4 col-form-label text-md-right">Harga Ruang Meeting Perjam</label>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                            <input id="harga_meeting_perjam" type="number" class="form-control{{ $errors->has('harga_meeting_perjam') ? ' is-invalid' : '' }} col-sm-9" name="harga_meeting_perjam" placeholder="Contoh: 100.000" value="{{ old('harga_meeting_perjam') }}" required>
                                                            @if ($errors->has('harga_meeting_perjam'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_meeting_perjam') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="harga_meeting_perhari" class="col-md-4 col-form-label text-md-right">Harga Ruang Meeting Perhari</label>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                            <input id="harga_meeting_perhari" type="number" class="form-control{{ $errors->has('harga_meeting_perhari') ? ' is-invalid' : '' }} col-sm-9" name="harga_meeting_perhari" placeholder="Contoh: 100.000" value="{{ old('harga_meeting_perhari') }}" required>
                                                            @if ($errors->has('harga_meeting_perhari'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_meeting_perhari') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="harga_meeting_perbulan" class="col-md-4 col-form-label text-md-right">Harga Ruang Meeting Perbulan</label>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                            <input id="harga_meeting_perbulan" type="number" class="form-control{{ $errors->has('harga_meeting_perbulan') ? ' is-invalid' : '' }} col-sm-9" name="harga_meeting_perbulan" placeholder="Contoh: 100.000" value="{{ old('harga_meeting_perbulan') }}" required>
                                                            @if ($errors->has('harga_meeting_perbulan'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_meeting_perbulan') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>                                                
                                                <p class="text-center">Ruang Event</p>
                                                <!-- Event Room -->
                                                <div class="form-group row">
                                                    <label for="harga_event_desc" class="col-md-4 col-form-label text-md-right">Catatan Ruang Event</label>
                                                    <div class="col-md-7">                                                        
                                                        <textarea class="form-control{{ $errors->has('harga_event_desc') ? ' is-invalid' : '' }}" rows="5" name="harga_event_desc" placeholder="Catatan tentang ruang event" required>{{ old('harga_event_desc') }}</textarea>
                                                        @if ($errors->has('harga_event_desc'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('harga_event_desc') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="harga_event_fasilitas" class="col-md-4 col-form-label text-md-right">Fasilitas Ruang Event</label>
                                                    <div class="col-md-7">
                                                        <textarea class="form-control{{ $errors->has('harga_event_fasilitas') ? ' is-invalid' : '' }}" rows="5" id="fasilitas_event" name="harga_event_fasilitas" required>{{ old('harga_event_fasilitas') }}</textarea>
                                                        @if ($errors->has('harga_event_fasilitas'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('harga_event_fasilitas') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="harga_event" class="col-md-4 col-form-label text-md-right">Harga Ruang Event</label>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                            <input id="harga_event" type="number" class="form-control{{ $errors->has('harga_event') ? ' is-invalid' : '' }} col-sm-9" name="harga_event" placeholder="Contoh: 100.000" value="{{ old('harga_event') }}" required>
                                                            @if ($errors->has('harga_event'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_event') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="image_coworking" class="col-md-4 col-form-label text-md-right">Upload Kelengkapan Harga</label>
                                                    <div class="col-md-7">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="customFile" name="harga_gambar">
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-daftar" role="button">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Content -->

    @include('partials.map.map')
@endsection