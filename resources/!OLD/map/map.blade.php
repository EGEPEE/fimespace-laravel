<script>
    var map, myLatLang, marker;
    geoLocationInit();
    searchmaps();

    function geoLocationInit(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(success, fail);
        }else{
            alert("Browser tidak mendukung.");
        }
    }

    function success(position){
        console.log(position);
        var latval = position.coords.latitude;
        var lngval = position.coords.longitude;
        myLatLang = new google.maps.LatLng(latval, lngval);
        $('#lat').val(latval);
        $('#lng').val(lngval);

        createMap(myLatLang);
        searchmaps();
    }

    function fail(){
        alert("gagal.")
        searchmaps();
    }

    function createMap(myLatLang){
        map =  new google.maps.Map(document.getElementById('map-canvas'),{
            center:myLatLang,
            zoom:15
        });
        marker =  new google.maps.Marker({
            position: myLatLang,
            map: map,
            draggable:true
        });
    }

    function searchmaps(){
        var searchBox= new google.maps.places.SearchBox(document.getElementById('searchmap'));

        google.maps.event.addListener(searchBox,'places_changed', function(){
            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for(i=0; place=places[i];i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }
            map.fitBounds(bounds);
            map.setZoom(15);
        });

        google.maps.event.addListener(marker, 'position_changed',function(){
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        })
    }

    function activatePlacesSearch(){
        var input = document.getElementById('kota_coworking');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
</script>

<!-- API Location -->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCyAGNSrdEm76jTm6T-BVmGcsfBgIhJRq4&libraries=places&callback=activatePlacesSearch"></script>