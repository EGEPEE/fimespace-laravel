@extends('layouts.master')
@section('title')
	Home
@endsection

@section('header')

    <!-- GLOBAL MANDATORY STYLES -->
    {{-- <link href="{{ asset('vendor/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/> --}}

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/swiper/css/swiper.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- THEME STYLES -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    @php
        //banner variable
        $img = 'url(../../assets/whoami.jpg)';
        $title = 'Selamat Datang di FimeSpace!';
        $caption = 'Pilih ruang kerja terbaikmu disini';
        $desc = 'Deskripsi';
    @endphp
    @include('partials.navbar.navbar')

    <!-- Banner -->
    @include('partials.banner')
	@include('partials.messages')
    
    <!-- Intro of FimeSpace -->
    <section id="whoami">
        <div class="container text-center">
            <div class="whoami-title">
                <h2>APA ITU FIMESPACE?</h2>
                <h3 class="section-subheading">Fime-Space atau Find and Make Coworking Space merupakan website yang dibentuk untuk mencari Coworking-Space yang dibutuhkan dengan suasana yang anda sukai untuk menyelesaikan tugas, membuat event seperti seminar maupun workshop. Selain itu, jika ada yang memiliki tempat ruang kosong dan bersedia untuk dijadikan coworking, dapat diperkenalkan disini.</h3>
            </div>
            <div class="whoami-button">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6">
                        <a href="{{ url('login') }}" class="btn btn-primary-second text-uppercase" role="button">Masuk</a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6">
                        <a href="{{ url('register') }}" class="btn btn-daftar text-uppercase" role="button">Daftar</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Counter -->
    <section id="facts" class="facts">
			<div class="parallax-overlay">
				<div class="container">
					<div class="row number-counters">
						<!-- first count item -->
						<div class="col-md-4 col-sm-4 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms">
							<div class="counters-item">
								<i class="fa fa-fort-awesome fa-3x"></i>
								<strong><span class="count">{{ count($coworkings) }}</span></strong>
								<!-- Set Your Number here. i,e. data-to="56" -->
								<p>Coworking Space</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
							<div class="counters-item">
								<i class="fa fa-rocket fa-3x"></i>
								<strong><span class="count">{{ $kota_co }}</span></strong>
								<!-- Set Your Number here. i,e. data-to="56" -->
								<p>Country</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 text-center wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="600ms">
							<div class="counters-item">
								<i class="fa fa-users fa-3x"></i>
								<strong><span class="count">{{ count($users) }}</span></strong>
								<!-- Set Your Number here. i,e. data-to="56" -->
								<p>Members</p>
							</div>
						</div>
						<!-- end first count item -->
					</div>
				</div>
			</div>
		</section>

		<!-- list coworking -->
		@if(count($coworkinglist)>=1)
			<section id="team" class="light-bg">
				<div class="overflow-h">
					<div class="content-lg container">
							<div class="row">
								<div class="col-lg-12 text-center">
									<div class="section-title">
										<h2 class="section-heading text-uppercase">Baru Bergabung</h2>
										<h3 class="section-subheading text-muted">Dibawah ini merupakan coworking space yang baru saja bergabung di FimeSpace. Ingin booking dan liat review-nya? Silahkan daftar akun-mu untuk melihat coworking yang kamu inginkan.</h3>
									</div>
								</div>
							</div>	
							<!-- Masonry Grid -->
							<div class="masonry-grid">		
									<div class="masonry-grid-sizer col-xs-6 col-sm-6 col-md-1"></div>
									<?php $count = 1; ?>
									@foreach ($coworkinglist as $coworking)
										@if($count == 5)
											@break
										@elseif($count==1)
											<div class="masonry-grid-item col-xs-12 col-sm-6 col-md-8">
													<!-- Work -->
													<div class="work wow fadeInUp" data-wow-duration=".3" data-wow-delay=".1s">
															<div class="work-overlay">
																	<img class="full-width img-responsive" src="/storage/assets/{{ $coworking->id_user }}/{{ $coworking->nama_co }}/logo_coworking/{{ $coworking->logo_co }}" alt="Logo of {{ $coworking->nama_co }}">
																	
															</div>
															<div class="work-content">																	
																<h3 class="text-uppercase color-white margin-b-5">{{ $coworking->nama_co }}</h3>
																<div class="team-location color-white"><span><i class="fa fa-map-marker"></i></span> {{ $coworking->kota_co }}</div>
															</div>
															<a class="content-wrapper-link" href="{{ url('coworking/'.$coworking->id_co) }}"></a>
													</div>
													<!-- End Work -->
											</div>
											@php
												$count++;
											@endphp
										@elseif($count==2)
											<div class="masonry-grid-item col-xs-6 col-sm-6 col-md-4">
													<!-- Work -->
													<div class="work wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
															<div class="work-overlay">
																<img class="full-width img-responsive" src="/storage/assets/{{ $coworking->id_user }}/{{ $coworking->nama_co }}/logo_coworking/{{ $coworking->logo_co }}" alt="Logo of {{ $coworking->nama_co }}">
															</div>
															<div class="work-content">																	
																<h3 class="text-uppercase color-white margin-b-5">{{ $coworking->nama_co }}</h3>
																<div class="team-location color-white"><span><i class="fa fa-map-marker"></i></span> {{ $coworking->kota_co }}</div>
															</div>
															<a class="content-wrapper-link" href="{{ url('coworking/'.$coworking->id_co) }}"></a>
													</div>
													<!-- End Work -->
											</div>
											@php
												$count++;
											@endphp
										@else
											<div class="masonry-grid-item col-xs-6 col-sm-6 col-md-4">
													<!-- Work -->
													<div class="work wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
															<div class="work-overlay">																
																<img class="full-width img-responsive" src="/storage/assets/{{ $coworking->id_user }}/{{ $coworking->nama_co }}/logo_coworking/{{ $coworking->logo_co }}" alt="Logo of {{ $coworking->nama_co }}">
															</div>
															<div class="work-content">
																	<h3 class="text-uppercase color-white margin-b-5">{{ $coworking->nama_co }}</h3>
																	<div class="team-location color-white"><span><i class="fa fa-map-marker"></i></span> {{ $coworking->kota_co }}</div>
															</div>															
															<a class="content-wrapper-link" href="{{ url('coworking/'.$coworking->id_co) }}"></a>
													</div>
													<!-- End Work -->
											</div>
											@php
												$count++;
											@endphp
										@endif
									@endforeach
							</div>
							<!-- End Masonry Grid -->
					</div>
				</div>
			</section>
		@endif
		
		<!-- Service -->
		<section id="services" class="bg-white">
			<div class="container">
				<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading text-uppercase">Cara Kerja Coworkers</h2>
					<h3 class="section-subheading text-muted">Mencari coworking Space di FimeSpace itu mudah dan aman.</h3>
				</div>
				</div>
				<div class="row text-center">
				<div class="col-md-4">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-get-pocket fa-stack-1x fa-inverse"></i>
					</span>
					<h4 class="service-heading">Buat Akun Pribadi</h4>
					<p class="text-muted">Buat akun pribadimu untuk mengakses fitur-fitur yang ada.</p>
				</div>
				<div class="col-md-4">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
					</span>
					<h4 class="service-heading">Lihat Coworking</h4>
					<p class="text-muted">Kamu dapat melihat-lihat deskripsi maupun informasi dan foto-foto dari tiap Coworking.</p>
				</div>
				<div class="col-md-4">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-lock fa-stack-1x fa-inverse"></i>
					</span>
					<h4 class="service-heading">Atur Jadwal</h4>
					<p class="text-muted">Kirim email kepada Coworking yang telah dipilih dan menyusun jadwal booking tempat.</p>
				</div>
				</div>
			</div>
		</section>
		
    @include('partials.about')
@endsection

@section('footer')

	<!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- CORE PLUGINS -->
	<script src="{{ asset('jquery/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('jquery/jquery-migrate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>

	<!-- PAGE LEVEL PLUGINS -->
	<script src="{{ asset('jquery-easing/jquery.easing.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('jquery/jquery.back-to-top.js') }}" type="text/javascript"></script>
	<script src="{{ asset('jquery/jquery.smooth-scroll.js') }}" type="text/javascript"></script>
	<script src="{{ asset('jquery/jquery.wow.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('vendor/swiper/js/swiper.jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('vendor/masonry/jquery.masonry.pkgd.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('vendor/masonry/imagesloaded.pkgd.min.js') }}" type="text/javascript"></script>

	<!-- PAGE LEVEL SCRIPTS -->
	<script src="{{ asset('js/layout.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/components/wow.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/components/swiper.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/components/masonry.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/agency.min.js') }}"></script>
@endsection