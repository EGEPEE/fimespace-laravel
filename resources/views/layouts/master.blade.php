<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta name="description" content="Untuk mencari informasi dan booking tempat di Coworking Space">
        <meta name="author" content="Ega Prasetianti">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>	
            
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
        
        <!-- Bootstrap -->
        <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Fontawesome -->
        <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        
        <!-- JQuery -->
        <script src="{{ asset('jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>

        @yield('header')
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/logo/icon.png') }}">
    </head>

    <body id="page-top">       
        
        @yield('content')
        
        <script>
        $(document).ready(function(){
            $("#informasi,#komunikasi,#lokasi,#fasilitas,#foto,#pembayaran,#harga,#simpan").hide();
            $(".service a").click(function(){
                hrefservice = $(this).attr('href');
                console.log(hrefservice);
        
                $('.form-content-choose').each(function(){
                    var ids = '#'+$(this).attr('id');
                    if(ids != hrefservice){
                        $(this).hide();
                    }else{
                        $(this).show();
                        $("#opening").hide();
                        $(this).removeClass("hidden");
                    }
                });
        
            });
            // navigation click actions 
            $('.scroll-link').on('click', function(event){
                event.preventDefault();
                var sectionID = $(this).attr("data-id");
                scrollToID('#' + sectionID, 750);
            });
            // scroll to top action
            $('.scroll-top').on('click', function(event) {
                event.preventDefault();
                $('html, body').animate({scrollTop:0}, 'slow');         
            });
            // mobile nav toggle
            $('#nav-toggle').on('click', function (event) {
                event.preventDefault();
                $('#main-nav').toggleClass("open");
            });
            // count
            $('.count').each(function () {
                $(this).prop('Counter',0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            $(window).scroll(function() {
                if ($(".navbar").offset().top > 50) {
                    $(".navbar-fixed-top").addClass("top-nav-collapse");
                } else {
                    $(".navbar-fixed-top").removeClass("top-nav-collapse");
                }
            });
        
            $('.custom-file-input').on('change', function() { 
                let fileName = $(this).val().split('\\').pop(); 
                $(this).next('.custom-file-label').addClass("selected").html(fileName); 
            });

            $("#harga-sewa ul").addClass("list-unstyled pricing-list margin-b-50");
            $("#harga-sewa ul li").addClass("pricing-list-item");
            $("#fasilitasco ul li").css({"list-style":"none", "font-size":"18px", "font-weight" : "500", "color": "#515769"});
            $("#fasilitasco ul li").prepend("<span><i class='fa fa-check-square fa-1x service-icon'></i></span> ");
            CKEDITOR.replace("article-ckeditor");
            CKEDITOR.replace("article-ckeditor2");
            CKEDITOR.replace("article-ckeditor3");
            CKEDITOR.replace("article-ckeditor4");
            
            CKEDITOR.config.fontSize_defaultLabel = '20';
        
            $('#datepicker').datepicker({
                changeMonth: true,
                changeYear: true
            });
        
            $('.alert').alert();
        
            $('a.page-scroll').bind('click', function(event) {
            var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top
                }, 1500, 'easeInOutExpo');
                event.preventDefault();
            });
        });
            // scroll function
        function scrollToID(id, speed){
            var offSet = 0;
            var targetOffset = $(id).offset().top - offSet;
            var mainNav = $('#main-nav');
            $('html,body').animate({scrollTop:targetOffset}, speed);
            if (mainNav.hasClass("open")) {
                mainNav.css("height", "1px").removeClass("in").addClass("collapse");
                mainNav.removeClass("open");
            }
        }
        if (typeof console === "undefined") {
            console = {
                log: function() { }
            };
        }
    </script>
        @yield('footer')
    </body>
</html>
