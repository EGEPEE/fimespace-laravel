@extends('layouts.master')
@section('title')
	Profil {{ $user->name }}
@endsection

@section('header')
    <!-- THEME STYLES -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    @include('partials.navbar.navbar')

    <!-- Header -->
    <header id="search-header" class="header-wrapper home-parallax home-fade random-header" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url( {{ asset('assets/bg-dashboard.jpg')}}) no-repeat center;background-size: cover; background-attachment: fixed;">
        <div class="header-overlay"></div>
        <div class="header-wrapper-inner">
            <div class="container">
                <div class="welcome-speech">
                    <h5>Halaman Utama Profil</h5>
                    <p class="text-uppercase"><u>{{ $user->name }}</u></p>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="profil-section">
            <div class="card">
                <div class="card-body">
                    <!-- Profil -->
                    <div class="profil">
                        @include('partials.messages')
                        <div class="row">
                            <div class="col-sm-12 col-md-3 marginBottom">
                                @if($user->foto_profil == null)
                                    <img class="img-thumbnail img-fluid" src="{{ asset('assets/noimage.png')}}" alt="photo">
                                @else
                                    <img class="img-thumbnail img-fluid" src="/storage/assets/{{ $user->id }}/foto_profil/{{ $user->foto_profil }}" alt="photo">
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-9">
                                <a href="{{ url('profile/'.$user->id.'/edit') }}" class="btn btn-primary marginBottom" role="button"><span><i class="fa fa-cog"></i></span> Ubah Profil</a>                          
                                <a href="{{ url('profile/booking/'.$user->id) }}" class="btn btn-daftar marginBottom" role="button"><span><i class="fa fa-bookmark"></i></span> Riwayat Pemesanan</a>
                                <div class="row">
                                        <h4 style="margin-left:2%;">{{ $user->name }}</h4>
                                        @if(($user->status == 0) && ($user->verifyToken != Null))
                                            <div class="col-sm-9">
                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                        <div class="container">
                                                            <strong>Peringatan!</strong> Silahkan verifikasi akun anda dengan email <b><u>{{$user->email}}</u></b>
                                                        </div>
                                                    </div>
                                            </div>
                                            @endif
                                         @if(($user->status == 1) && ($user->verifyToken == Null))
                                            <div class="col-2" style="margin-top:-10px;">
                                                 <div class="verifikasi">
                                                    <img src="{{ asset('assets/checked.png') }}" alt="verifikasi" style="width:17px;height:17px;">
                                                 </div>
                                            </div>
                                        @endif
                                </div>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <p><span><i class="fa fa-envelope"></i></span> Email</p>
                                            </td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><span><i class="fa fa-phone"></i></span> Nomor Telepon</p>
                                            </td>
                                            <td>{{ $user->no_telp }}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><span><i class="fa fa-home"></i></span> Alamat</p>
                                            </td>
                                            <td>{{ $user->alamat }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="marginBottom">
            @if(count($data) < 1)
                <div class="card">
                    <div class="card-body">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                {{--  @include('partials.messages')  --}}
                                <h5>Belum Ada Coworking Space</h5>
                                <a href="{{ url('coworking/profile/createco', Auth::user()->id) }}" class="btn btn-primary btn-daftar" role="button">Buat Coworking ?</a>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                @foreach($data as $data)
                    @if(Auth::user()->id == $data->id_user)
                        <div class="card">
                            <div class="card-body">
                                <!-- Coworking Space -->
                                <div class="own-coworking">
                                    <h5 class="marginBottom">Coworking Space Anda: {{ $data->nama_co }}</h5>
                                    <div class="row">
                                        <div class="col-md-3">
                                        <img class="img-thumbnail img-fluid" src="/storage/assets/{{ $data->id_user }}/{{$data->nama_co}}/logo/{{$data->logo_co}}" alt="Logo dari {{ $data->nama_co }}"/>        
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row text-center">
                                                <div class="col-sm-4">
                                                    <a href="{{ url('coworking/profile/'.$data->id_co ) }}" class="btn" role="button">        
                                                        <span class="fa-stack fa-4x">
                                                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                            <i class="fa fa-building fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                        <h6 class="service-heading">Lihat Coworking</h6>
                                                    </a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a href="{{ url('coworking/booking/'.$data->id_co) }}" class="btn" role="button">
                                                        <span class="fa-stack fa-4x">
                                                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                            <i class="fa fa-list-alt fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                        <h6 class="service-heading">Daftar Pemesanan</h6>
                                                    </a>
                                                </div>                                    
                                                <div class="col-sm-4">
                                                    <a href="{{ url('coworking/profile/'.$data->id_co.'/edit') }}" class="btn" role="button">
                                                        <span class="fa-stack fa-4x">
                                                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                                            <i class="fa fa-cogs fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                        <h6 class="service-heading">Edit Coworking</h6>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
@endsection

@section('footer')<script>
        $(document).ready(function(){
        $('.count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(window).scroll(function() {
            if ($(".navbar").offset().top > 50) {
                $(".navbar-fixed-top").addClass("top-nav-collapse");
            } else {
                $(".navbar-fixed-top").removeClass("top-nav-collapse");
            }
        });
        $('.custom-file-input').on('change', function() { 
        let fileName = $(this).val().split('\\').pop(); 
        $(this).next('.custom-file-label').addClass("selected").html(fileName); 
        });
        CKEDITOR.replace('article-ckeditor');
        $('#datepicker').datepicker({
            changeMonth: true,
            changeYear: true
        });
        $('.alert').alert();
        $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });
    </script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/agency.min.js') }}"></script>
@endsection