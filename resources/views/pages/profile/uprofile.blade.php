@extends('layouts.master')
@section('title')
	Edit Profil {{ $user->name }}
@endsection

@section('header')
    {{-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script> --}}
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>    
@endsection

@section('content')
    @include('partials.navbar.navbar')

    <section class="container">
        <div class="row">
            <div class="col-12 col-md-3 marginBottom">
                <div class="card">
                    <div class="card-body">
                        <div class="buat-co">
                            <h5>{{ $user->name }}</h5>
                            <p>Bergabung: <br>{{ $user->created_at }}</p>
                            <hr>
                            @if(count($data) < 1)
                                <p>Belum Ada Coworking</p>
                                <a href="{{ url('coworking/profile/create') }}" class="btn btn-primary" role="button">Buat Coworking</a>
                            @else
                                @foreach($coworking as $coworking)
                                    @if(Auth::user()->id == $coworking->id_user)
                                        <a href="{{ url('coworking/profile/'.$coworking->id_co) }}" class="btn btn-primary center" role="button">{{ $coworking->nama_co }}</a>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9">
                <div class="card">
                    <div class="card-body">
                        <h6>Ubah Biodata Diri</h6>
                        @include('partials.messages')
                        <div class="row">
                            <div class="col-12 col-md-4 marginBottom">
                                <div class="card text-muted container">
                                        @if($user->foto_profil == null)
                                            <img src="{{ asset('assets/noimage.png')}}" class="img-fluid card-img-top logo_dashboard" alt="Foto User">
                                        @else
                                            <img src="/storage/assets/{{ $user->id }}/foto_profil/{{ $user->foto_profil }}" class="img-fluid card-img-top logo_dashboard" alt="Foto User">
                                        @endif
                                        <!-- form input -->
                                    <form action="{{ url('profile/'.$user->id)}}" method="POST" enctype="multipart/form-data" file="true">
                                        <input type="hidden" name="_method" value="put">
                                        @csrf
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" value="{{ old('foto_profil') }}" name="foto_profil">
                                        <label class="custom-file-label" for="customFile">{{ old('foto_profil') }}</label>
                                        @if ($errors->has('foto_profil'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('foto_profil') }}</strong>
                                            </span>
                                        @endif  
                                    </div>
                                    <div>
                                        <div class="container">
                                            <label class="col-form-label">Besar file: maksimum 1000 bytes (1 Megabytes)</label>
                                            <label class="col-form-label">Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-8">
                                    <div class="form-group row">
                                        <label for="name" class="col-md-3 col-form-label">Nama</label>
                                        <div class="col-md-9">
                                        <input id="text" type="text" class="form-control" name="name" value="{{ $user->name }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_telp" class="col-md-3 col-form-label">Nomor HP</label>
                
                                        <div class="col-md-9">
                                        <input id="text" type="number" class="form-control" name="no_telp" value="{{ $user->no_telp }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="date" class="col-md-3 col-form-label">Jenis Kelamin</label>
                
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <div class="input-group mb-3">
                                                    <select class="custom-select" id="jk" name="jk" value="{{ $user->jk }}">
                                                        <option selected>{{ $user->jk }}</option>
                                                        <option value="L">Laki-Laki</option>
                                                        <option value="P">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="alamat" class="col-md-3 col-form-label">Alamat</label>
                
                                        <div class="col-md-9">
                                            <textarea class="form-control" rows="5" name="alamat" required>{{ $user->alamat }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-1 col-lg-5"> </div>
                    
                                        <div class="col-md-11 col-lg-7">
                                            <button type="submit" class="btn btn-daftar" role="button">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@endsection