@extends('layouts.master')
@section('title')
	Edit Profil {{ $user->name }}
@endsection

@section('header')
    {{-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script> --}}
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>    
@endsection

@section('content')
    @include('partials.navbar.navbar')

    <!-- Header -->
    <header id="search-header" class="header-wrapper home-parallax home-fade random-header" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url( {{ asset('assets/bg-dashboard.jpg')}}) no-repeat center;background-size: cover; background-attachment: fixed;">
        <div class="header-overlay"></div>
        <div class="header-wrapper-inner">
            <div class="container">
                <div class="welcome-speech">
                    <p class="text-uppercase"><u>Pengaturan Akun</u></p>
                </div>
            </div>
        </div>
    </header>

    <section class="container">
        @include('partials.messages')
        <div class="row">
            <div class="col-12 col-md-3 marginBottom">
                <div class="card">
                    <div class="card-body">
                        <div class="buat-co">
                            <h5>{{ $user->name }}</h5>
                            <hr>
                            <p>Bergabung: <br>{{ $user->created_at }}</p>
                            <hr>
                            <p>Pengaturan Profil</p>
                            <a href="{{ url('profile/'.$user->id.'/edit') }}" class="btn btn-daftar" role="button">Ubah Profil</a>                
                            <hr>
                            <p>Coworking Space</p>
                            @if(count($data) < 1)
                                <p>Belum Ada Coworking</p>
                                <a href="{{ url('coworking/profile/create') }}" class="btn btn-daftar" role="button">Buat Coworking</a>
                            @else
                                @foreach($coworking as $coworking)
                                    @if(Auth::user()->id == $coworking->id_user)
                                        <a href="{{ url('coworking/profile/'.$coworking->id_co) }}" class="btn btn-primary center" role="button">{{ $coworking->nama_co }}</a>
                                    @endif
                                @endforeach
                            @endif          
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ url('profile/setting-update/'.$user->id)}}" method="POST">
                            <input type="hidden" name="_method" value="put">
                            @csrf
                            <h5>Nama</h5>
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label">Nama</label>
                                <div class="col-md-9">
                                <input id="nama" type="text" class="form-control" name="name" value="{{ $user->name }}">
                                </div>
                            </div>
                            <hr>
                            <h5>Ganti Email</h5>
                            <div class="form-group row">
                                <label for="email" class="col-md-3 col-form-label">Email</label>
        
                                <div class="col-md-9">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">
                                </div>
                            </div>
                            <hr>
                            <h5>Ganti Password</h5>
                            <div class="form-group row">
                                <label for="Password Lama" class="col-md-3 col-form-label">Password Lama</label>
                                <div class="col-md-9">
                                <input id="pass_lama" type="password" class="form-control" name="pass_lama" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Password Lama" class="col-md-3 col-form-label">Password Baru</label>
                                <div class="col-md-9">
                                <input id="pass_baru" type="password" class="form-control" name="pass_baru" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Password Lama" class="col-md-3 col-form-label">Konfirmasi Password</label>
                                <div class="col-md-9">
                                    <input id="konfirmasi_pass" type="password" class="form-control" name="konfirmasi_pass" value="">
                                    <span id='message'></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-1 col-lg-5"> </div>
            
                                <div class="col-md-11 col-lg-7">
                                    <button id="btn_simpan" type="submit" class="btn btn-daftar" role="button" enabled>Simpan Perubahan</button>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <h5>Hapus Akun</h5>
                        <div class="row">
                            <label for="Password Lama" class="col-md-3 col-form-label">Hapus Akun</label>
                            <div class="col-md-9">
                                <a class="btn btn-primary" href="#" onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                                    <h6>Hapus Akun</h6>
                                </a>
                            </div>
                            <form id="delete-form" method="post" action="{{ url('profile', Auth::user()->id) }}">
                                <input type="hidden" name="_method" value="delete" />
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('footer')
    <script>
    $('#pass_baru, #konfirmasi_pass').on('keyup', function () {
    if ($('#pass_baru').val() == $('#konfirmasi_pass').val()) {
        $('#message').html('Password Cocok.').css('color', 'green');
        $(":submit").removeAttr('disabled');
    } else {
        $('#message').html('Password Tidak Cocok.').css('color', 'red');
        $(":submit").attr("disabled", true);
    }
    });
    </script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@endsection