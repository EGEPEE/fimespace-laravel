@extends('layouts.master')

@section('title')
    Daftar Riwayat Pemesanan: {{ $user->name }}
@endsection

@section('header')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    @include('partials.navbar.navbar')
    <!-- Header -->
    <header id="search-header" class="header-wrapper home-parallax home-fade random-header" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url( {{ asset('assets/bg-dashboard.jpg')}}) no-repeat center;background-size: cover; background-attachment: fixed;">
        <div class="header-overlay"></div>
            <div class="header-wrapper-inner">
            <div class="container">
                <div class="welcome-speech">
                    <h5>List Daftar Riwayat Pemesanan</h5>
                    <p class="text-uppercase"><u>{{ $user->name }}</u></p>
                    {{-- <p class="text-uppercase"><a href="{{ url('coworking/profile/'.$user->id) }}" role="button"><u>{{ $user->name }}</u></a></p> --}}
                </div><!-- /.intro -->
            </div>
        </div>
    </header>

    <!-- Table Daftar pemesanan User -->
    <div class="booking">
        @include('partials.messages')
        <div class="container">
            <table class="table table-bordered table-hover table-responsive">
                <thead class="text-center thead-dark">
                    <tr>
                        <th>Atas Nama</th>
                        <th>Coworking Space</th>
                        <th>Tanggal Sewa</th>
                        <th>Jenis Ruang</th>
                        <th>Jenis Waktu</th>
                        <th>Lama Sewa</th>
                        <th>Status Pemesanan</th>
                        <th>Status Bayar</th>
                        <th>Detail Pilihan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($datas as $data)
                        <tr>
                            <td>{{ $data->nama }}</td>
                            <td>
                                <a href="{{ url('coworking/profile/'.$data->id_co) }}" role="button">{{ $data->nama_co }}</a>
                            </td>
                            <td>{{ $data->tgl_sewa }}</td>
                            <td>{{ $data->jenis_ruang }}</td>
                            <td>{{ $data->jenis_waktu }}</td>
                            <td>{{ $data->waktu_sewa }}</td>
                            <td>{{ $data->status_booking }}</td>
                            <td>{{ $data->status_bayar }}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#modal{{$data->id_booking}}" class="btn btn-primary" role="button">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $datas->links() }}
        </div>
    </div>

    <!-- Modal Detail -->
    @foreach($datas as $data)      
        <div class="modal fade" id="modal{{$data->id_booking}}" tabindex="-1" role="dialog" aria-labelledby="modal{{$data->id_booking}}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Modal-label-1">Detail Pemesanan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2">Detail Pemesanan Coworking Space: <u>{{ $data->nama_co }}</u> pada tanggal reservasi: <u>{{ $data->created_at }}</u></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Atas Nama</td>
                                        <td class="text-uppercase">{{ $data->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>ID Pemesanan</td>
                                        <td>{{ $data->id_booking }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Coworking</td>
                                        <td>{{ $data->nama_co }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Sewa</td>
                                        <td>{{ $data->tgl_sewa }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Ruangan</td>
                                        <td>{{ $data->jenis_ruang }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Waktu Sewa</td>
                                        <td>{{ $data->jenis_waktu }}</td>
                                    </tr>
                                    <tr>
                                        <td>Lama Sewa</td>
                                        <td>{{ $data->waktu_sewa }}</td>
                                    </tr>
                                    <tr>
                                        <td>Banyak Orang</td>
                                        <td>{{ $data->banyak_orang }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total Bayar</td>
                                        <td>{{ $data->total_bayar }}</td>
                                    </tr>  
                                    <tr>
                                        <td>Status Bayar</td>
                                        <td>{{ $data->status_bayar }}</td>
                                    </tr>  
                                    <tr>
                                        <td>Status Pemesanan</td>
                                        <td>{{ $data->status_booking }}</td>
                                    </tr>  
                                    <tr>
                                        <td>Catatan Pemesanan</td>
                                        <td>{!! $data->pesan !!}</td>
                                    </tr>                                    
                                    <tr>
                                        <td>Testimoni</td>
                                        <td>
                                            @if($data->testimoni == null)
                                                @if(($data->status_booking == 'Selesai') && ($data->status_bayar == 'Lunas'))
                                                    <form action="{{ url('coworking/booking/'.$data->id_booking)}}" method="POST" enctype="multipart/form-data" file="true">
                                                        <input type="hidden" name="_method" value="put">
                                                        @csrf
                                                        <div class="form-group">   
                                                            <textarea class="form-control{{ $errors->has('ulasan') ? ' is-invalid' : '' }}" rows="5" name="ulasan" placeholder="Tuliskan ulasan Coworking Space {{ $data->nama_co }}" required>{{ old('ulasan') }}</textarea>
                                                            @if ($errors->has('ulasan'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('ulasan') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                        <input id="datepicker" type="date" class="form-control" name="tgl_testimoni" required>                                                
                                                        
                                                        
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary pull-right" role="button">Simpan</button>
                                                        </div>
                                                    </form>
                                                @else
                                                    Dapat mengisi setelah melakukan pembayaran dan penyewaan.
                                                @endif
                                            @else
                                                {{ $data->testimoni }}
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('footer')
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/agency.min.js') }}"></script>
@endsection