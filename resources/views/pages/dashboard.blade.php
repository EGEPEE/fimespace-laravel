@extends('layouts.master')

@section('title')
	Dashboard
@endsection

@section('header')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <style>
        .customMarker {
            position:absolute;
            cursor:pointer;
            background:#17bed2;
            width:40px;
            height:40px;
            /* -width/2 */
            margin-left:-25px;
            /* -height + arrow */
            margin-top:-50px;
            border-radius:50%;
            padding:0px;
        }
        .customMarker:after {
            content:"";
            position: absolute;
            bottom: -15px;
            left: 13px;
            border-width: 10px 10px 0;
            border-style: solid;
            border-color: #17bed2 transparent;
            display: block;
            width: 0;
        }
        .customMarker img {
            width:30px;
            height:30px;
            margin:5px;
            border-radius:50%;
        }
    </style>
@endsection

@section('content')
    @include('partials.navbar.navbar')
    <!-- Header -->
    <header id="search-header" class="header-wrapper home-parallax home-fade random-header" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url( {{ asset('assets/bg-dashboard.jpg')}}) no-repeat center;background-size: cover; background-attachment: fixed;">
        <div class="header-overlay"></div>
        <div class="header-wrapper-inner">
            <div class="container">
                <div class="welcome-speech">
                    <h5>Cari Coworking</h5>
                    <form action="{{ url('dashboard/search/')}}" method="GET" class="container">
                        @csrf
                        <div class="form-group row">
                            <input id="search_co" type="text" class="form-control col-10 col-sm-11" name="search_co" placeholder="Masukkan Nama Coworking, Alamat atau Kota..." required>
                            <button type="submit" class="btn btn-primary col-2  col-sm-1" role="button"><span><i class="fa fa-search"></i></span></button>
                        </div>
                    </form> 
                    @if($search == null)
                        <p></p>
                    @else
                        <h4>Hasil Pencarian: <b class="text-uppercase">{{ $search }}</b></h4>
                        <a class="btn btn-primary btn-default" role="button" href="{{ url('dashboard') }}"><b>Tampilkan Coworking</b></a>
                    @endif
                    @include('partials.messages')
                </div>
            </div>
        </div>
    </header>

    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                    </div>
                </div>
            </div>
            <div class="row row-0-gutter container">
                @if(count($datas) >=1)
                    @foreach($datas as $data)
                        <!-- start portfolio item -->
                        <div class="col-md-4 col-0-gutter dash-col">
                            <div class="ot-portfolio-item">
                                <figure class="effect-bubba">
                                    <img class="logo_dashboard" src="/storage/assets/{{ $data->id_user }}/{{ $data->nama_co }}/logo/{{ $data->logo_co }}" alt="Photo of {{ $data->nama_co }}"/>
                                    <figcaption>
                                        <h2>{{ $data->nama_co }}</h2>
                                        <p>{{ $data->kota }}</p>
                                        <a  href="#" data-toggle="modal" data-target="#modal{{$data->id_co}}" class="btn btn-primary-second text-uppercase" role="button">Selengkapnya</a>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <!-- end portfolio item -->
                    @endforeach
                    {{ $datas->links() }}
                @else
                    <div class="col-lg-12">
                        <div class="section-title text-center">
                            <h2>Tidak Ada Coworking Space Yang Terdaftar</h2>
                        </div>
                    </div>
                @endif
            </div>
        </div><!-- end container -->
    </section>

    <!-- Google Maps -->
    <div>
        <div id="map" style="width: 640pxpx; height: 480px;">
            <div class="text-center">
                <h2>Cek koneksi atau refresh page agar dapat menampilkan maps</h2>
            </div>
        </div>
    </div>

    @foreach($datas as $data)
        <div class="modal fade" id="modal{{$data->id_co}}" tabindex="-1" role="dialog" aria-labelledby="modal{{$data->id_co}}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Modal-label-1">{{ $data->nama_co }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                    </div>
                    <div class="modal-body">
                        <img src="/storage/assets/{{ $data->id_user }}/{{$data->nama_co}}/logo/{{$data->logo_co}}" alt="Photo of {{ $data->nama_co }}" class="logo_dashboard" />
                        <div class="modal-works"><span><i class="fa fa-map-marker"></i> {{ $data->kota }}</span></div>
                        <p>{{ $data->alamat_co }}</p>
                        <hr>
                        <p>{{ $data->desc }}</p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ url('coworking/profile/'.$data->id_co) }}" class="btn btn-primary" role="button">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('footer')
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCyAGNSrdEm76jTm6T-BVmGcsfBgIhJRq4&libraries=places&callback=geoLocationInit"></script>
    <script>
    var map, myLatLang, marker;
        geoLocationInit();

        function geoLocationInit(){
            if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(success, fail);
            }else{
                alert("Browser tidak mendukung.");
            }
        }

        function success(position){
            console.log(position);
            var latval = position.coords.latitude;
            var lngval = position.coords.longitude;
            myLatLang = new google.maps.LatLng(latval, lngval);
            $('#lat').val(latval);
            $('#lng').val(lngval);

            createMap(myLatLang);
        }

        function fail(){
            alert("Silahkan cek jaringan anda dan refresh halaman kembali.");
            myLatLang = new google.maps.LatLng(-6.175392, 106.827153);
            createMap(myLatLang);
        }

        function createMap(myLatLang){
            var coworking = @json($datas);
            var data = coworking.data;
            console.log(data);

            function CustomMarker(latlng, map, imageSrc, url, id_co) {
                this.latlng_ = latlng;
                this.imageSrc = imageSrc;
                this.url = url;
                this.id_co = id_co;
                this.setMap(map);
            }

            CustomMarker.prototype = new google.maps.OverlayView();

            CustomMarker.prototype.draw = function () {
                // Check if the div has been created.
                var div = this.div_;
                if (!div) {
                    // Create a overlay text DIV
                    div = this.div_ = document.createElement('div');
                    // Create the DIV representing our CustomMarker
                    div.className = "customMarker";
                    div.setAttribute("id", "customMarker"+this.id_co)
                    
                    var link = document.createElement("a");
                    var img = document.createElement("img");
                    // link.setAttribute("href", this.url);
                    link.setAttribute("data-toggle", "modal");
                    link.setAttribute("data-target", "#modal"+this.id_co);
                    img.setAttribute("src", this.imageSrc);
                    link.appendChild(img);
                    div.appendChild(link);

                    console.log(div.id + " = (modal) " + "modal"+this.id_co);
                    // google.maps.event.addDomListener(div, "click", function (event) {
                    //     google.maps.event.trigger(me, "click");
                    // });

                    //MARKER
                    var infoWindow = new google.maps.InfoWindow();  
                    var marker1 = div.id;
                    var content = '<div style="text-align: center; font-size:14px;"><center><b>Company GmbH</b></center><div>Broadway Str.5</div><div>45132 Canvas</div></div>';

                    // Then add the overlay to the DOM
                    var panes = this.getPanes();
                    panes.overlayImage.appendChild(div);

                    infoWindow.setContent(content);
                    infoWindow.open(map, marker1);

                    google.maps.event.addDomListener(marker1, 'click', openInfoWindow);
                }

                // Position the overlay 
                var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
                if (point) {
                    div.style.left = point.x + 'px';
                    div.style.top = point.y + 'px';
                }
            };

            CustomMarker.prototype.remove = function () {
                // Check if the overlay was on the map and needs to be removed.
                if (this.div_) {
                    this.div_.parentNode.removeChild(this.div_);
                    this.div_ = null;
                }
            };

            CustomMarker.prototype.getPosition = function () {
                return this.latlng_;
            };

            var map = new google.maps.Map(document.getElementById("map"), {
                zoom: 17,
                center: myLatLang,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({  
                map: map, 
                position: myLatLang
            }); 

            for(var i=0;i<=data.length;i++){
                new CustomMarker(new google.maps.LatLng(data[i].lat, data[i].lng), map,  '/storage/assets/'+data[i].id_user+'/'+data[i].nama_co+'/logo/'+data[i].logo_co, 'coworking/profile/'+data[i].id_co, data[i].id_co) 
            }
        }
    </script>
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('jquery/jquery1.11.0-ui.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('js/agency.min.js') }}"></script>
@endsection