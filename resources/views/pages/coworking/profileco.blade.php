@extends('layouts.master')
@section('title')
	Profil Coworking - {{ $coworking->nama_co }}
@endsection

@section('header')
    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/swiper/css/swiper.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- THEME STYLES -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    @include('partials.navbar.navbar')
    <!-- Header-->
    @php
        $namaco = str_replace(' ', '%20', $coworking->nama_co);
    @endphp
    <header id="coworking-header" class="header-wrapper home-parallax home-fade random-header" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url({{ URL::asset('/storage/assets/'.$coworking->id_user.'/'.$namaco.'/header/'.$coworking->header_co) }}) no-repeat center;background-size: cover; background-attachment: fixed;">
        <div class="header-overlay"></div>
        <div class="header-wrapper-inner">
            <div class="container">
                <h3 class="text-uppercase"><u>{{ $coworking->nama_co }}</u></h3>
                <h5 class="marginBottom"><span><i class="fa fa-map-marker"></i> {{ $coworking->alamat_co }}</span></h5>
                <div class="social-media">
                    <div class="row">
                        <div class="col-3">
                            <a href="{{ $coworking->fb }}" target="_blank"><span><i class="fa fa-facebook-square fa-3x"></i></span></a>
                        </div>
                        <div class="col-3">
                            <a href="{{ $coworking->twitter }}" target="_blank"><span><i class="fa fa-twitter-square fa-3x"></i></span></a>
                        </div>
                        <div class="col-3">
                            <a href="{{ $coworking->ig }}" target="_blank"><span><i class="fa fa-instagram fa-3x"></i></span></a>
                        </div>
                        <div class="col-3">
                            <a href="{{ $coworking->website }}" target="_blank"><span><i class="fa fa-at fa-3x"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Thumbnails -->
    <div id="thumbnails" class="bg-white" data-auto-height="true">
        <div class="about-choose container">
            @include('partials.messages')
            <div class="row row-space-1 margin-b-2 container">
                <div class="col-sm-4 sm-margin-b-2">
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                        <div class="service" data-height="height">
                            <div class="service-element">
                                <span><i class="fa fa-bullhorn service-icon"></i></span>
                            </div>
                            <div class="service-info">
                                <h5>Deskripsi</h5>
                            </div>
                            <a href="#deskripsi" class="content-wrapper-link"></a>    
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 sm-margin-b-2">
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                        <div class="service" data-height="height">
                            <div class="service-element">
                                <span><i class="fa fa-check-circle service-icon"></i></span>
                            </div>
                            <div class="service-info">
                                <h5>Fasilitas</h5>
                            </div>
                            <a href="#fasilitasco" class="content-wrapper-link"></a>    
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                        <div class="service" data-height="height">
                            <div class="service-element">
                                <span><i class="fa fa-credit-card service-icon"></i></span>
                            </div>
                            <div class="service-info">
                                <h5>Harga Sewa</h5>
                            </div>
                            <a href="#harga-sewa" class="content-wrapper-link"></a>    
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-space-1 margin-b-2 container">
                <div class="col-sm-4 sm-margin-b-2">
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".5s">
                        <div class="service" data-height="height">
                            <div class="service-element">
                                <span><i class="fa fa-braille service-icon"></i></span>
                            </div>
                            <div class="service-info">
                                <h5>Ulasan</h5>
                            </div>
                            <a href="#ulasan" class="content-wrapper-link"></a>    
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 sm-margin-b-2">
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".4s">
                        <div class="service" data-height="height">
                            <div class="service-element">
                                <span><i class="fa fa-map-marker service-icon"></i></span>
                            </div>
                            <div class="service-info">
                                <h5>Lokasi</h5>
                            </div>
                            <a href="#lokasico" class="content-wrapper-link"></a>    
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".6s">
                        <div class="service" data-height="height">
                                <div class="service-element">
                                    <span><i class="fa fa-comments service-icon"></i></span>
                                </div>
                            <div class="service-info">
                                <h5>Kontak Kami</h5>
                            </div>
                            <a href="#kontak" class="content-wrapper-link"></a>    
                        </div>
                    </div>
                </div>
            </div>

            @if(Auth::user()->id == $coworking->id_user)
                <div class="row row-space-1 container">
                    <div class="col-sm-4 sm-margin-b-2">
                        <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".4s">
                            <div class="service" data-height="height">
                                <div class="service-element">
                                    <span><i class="fa fa-list-alt service-icon"></i></span>
                                </div>
                                <div class="service-info">
                                    <h5>Lihat Pemesanan</h5>
                                </div>
                                <a href="{{ url('coworking/booking/'.$coworking->id_co) }}" class="content-wrapper-link"></a>    
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 sm-margin-b-2">
                        <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".5s">
                            <div class="service" data-height="height">
                                <div class="service-element">
                                    <span><i class="fa fa-cogs service-icon"></i></span>
                                </div>
                                <div class="service-info">
                                    <h5>Edit Data</h5>
                                </div>
                                <a href="{{ url('coworking/profile/'.$coworking->id_co.'/edit') }}" class="content-wrapper-link"></a>    
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 sm-margin-b-2">
                        <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".5s">
                            <div class="service" data-height="height">
                                <div class="service-element">
                                    <span><i class="fa fa-cogs service-icon"></i></span>
                                </div>
                                <div class="service-info">
                                    <h5>Hapus Coworking</h5>
                                </div> 
                                <a onclick="event.preventDefault(); document.getElementById('delete-form').submit();" class="content-wrapper-link"></a>  
                                <form id="delete-form" method="post" action="{{ url('coworking/profile/'.$coworking->id_co) }}">
                                    <input type="hidden" name="_method" value="delete" />
                                    @csrf
                                </form>  
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <!-- Deskripsi -->
    <div id="deskripsi" class="about-section">
        <div class="container">
            <div class="row marginBottom">
                <div class="col-sm-6">
                    <h2>Deskripsi</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-1">
                    <img class="profilco img-thumbnail img-fluid" src="/storage/assets/{{ $coworking->id_user }}/{{$coworking->nama_co}}/logo/{{$coworking->logo_co}}" alt="Logo dari {{ $coworking->nama_co }}">
                </div>
                <div class="col-sm-6 marginBottom">
                    <div class="margin-b-30">
                        <p>{{ $coworking->desc }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Fasilitas -->
    <div id="fasilitasco" class="about-section bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <h2>Fasilitas</h2>
                    <p>Fasilitas yang di sediakan oleh {{ $coworking->nama_co }} :</p>
                </div>
                <div class="col-sm-7">
                    <p>{!! $coworking->fasilitas !!}</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Harga Sewa -->
    <div id="harga-sewa">
        <div class="container">
            <div class="about-section">
                <div class="row row-space-1">
                    <!-- Ruang Coworking -->
                    <div class="col-sm-12 col-md-4 col-lg-4 sm-margin-b-2">
                        <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                            <!-- Pricing -->
                            <div class="pricing">
                                <div class="marginBottom">
                                    <span><i class="fa fa-cubes pricing-icon"></i></span>
                                    <h3>Ruang Coworking</h3>
                                    <p>{{ $coworking->coworking_desc }}</p>
                                </div>
                                <ul class="list-unstyled pricing-list marginBottom">
                                    <li class="pricing-list-item">Rp. {{ $coworking->coworking_perhari }},- /hari</li>
                                    @if( $coworking->coworking_perbulan != null )
                                    <li class="pricing-list-item">Rp. {{ $coworking->coworking_perbulan }},- /bulan</li>
                                    @endif
                                    @if( $coworking->coworking_pertahun != null )
                                    <li class="pricing-list-item">Rp. {{ $coworking->coworking_pertahun }},- /tahun</li>
                                    @endif
                                </ul>
                                <a  href="#" data-toggle="modal" data-target="#modal-coworking" class="btn btn-primary  text-uppercase" role="button">Sewa Ruangan</a>
                            </div>
                            <!-- End Pricing -->
                        </div>
                    </div>
    
                    <!-- Ruang Meeting dan Diskusi -->
                    <div class="col-sm-12 col-md-4 col-lg-4 sm-margin-b-2">
                        <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
                            <!-- Pricing -->
                            <div class="pricing pricing-active">
                                <div class="margin-b-30">
                                    <span><i class="fa fa-link pricing-icon"></i></span>
                                    <h3>Ruang Meeting dan Diskusi</h3>
                                    <p>{{ $coworking->meeting_desc }}</p>
                                </div>
                                @if($coworking->meeting_perjam == null && $coworking->meeting_perhari == null && $coworking->meeting_perbulan == null)
                                    <div>
                                        <p>Tidak ada ruang meeting dan diskusi</p>
                                    </div>
                                @else
                                    <ul class="list-unstyled pricing-list marginBottom">
                                        @if( $coworking->meeting_perjam != null )
                                            <li class="pricing-list-item">Rp. {{ $coworking->meeting_perjam }},- /jam</li>
                                        @endif
                                        @if( $coworking->meeting_perhari != null )
                                        <li class="pricing-list-item">Rp. {{ $coworking->meeting_perhari }},- /hari</li>
                                        @endif
                                        @if( $coworking->meeting_perbulan != null )
                                        <li class="pricing-list-item">Rp. {{ $coworking->meeting_perbulan }},- /bulan</li>
                                        @endif
                                    </ul>                            
                                    <a  href="#" data-toggle="modal" data-target="#modal-meeting" class="btn btn-primary text-uppercase" role="button">Sewa Ruangan</a>
                                @endif
                            </div>
                            <!-- End Pricing -->
                        </div>
                    </div>
    
                    <!-- Ruang Acara -->
                    <div class="col-sm-12 col-md-4 col-lg-4 ">
                        <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".1s">
                            <!-- Pricing -->
                            <div class="pricing">
                                <div class="margin-b-30">                                
                                    <span><i class="fa fa-ticket pricing-icon"></i></span>
                                    <h3>Event Space</h3>
                                    @if($coworking->event_harga == null)
                                    <div>
                                        <p>Coworking space tidak menyediakan event space.</p>
                                    </div>
                                @else
                                    <p>{!! $coworking->event_desc !!} </p>
                                    <p>Rp. {{ $coworking->event_harga }},- /jam</p>
                                </div>                         
                                <a  href="#" data-toggle="modal" data-target="#modal-event" class="btn btn-primary text-uppercase" role="button">Sewa Ruangan</a>
                                @endif
                            </div>
                            <!-- End Pricing -->
                        </div>
                    </div>
                </div>
                <!--// end row -->
            </div>
        </div>
    </div>

    <!-- Ulasan -->
    <div id="ulasan" class="about-section bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-11">
                            <h2 class="marginBottom">Ulasan</h2>
                        </div>
                        <div class="col-sm-1">
                            <a  href="#" data-toggle="modal" data-target="#modal-testi" class="btn btn-primary float-right" role="button"><span><i class="fa fa-braille"></i></span></a>
                        </div>
                    </div>
                    <div class="swiper-slider swiper-testimonials">
                        <div class="swiper-wrapper">
                            @if(count($datas) < 1)
                            <div class="text-center">
                                <h5>Belum ada ulasan</h5>
                            </div>
                            @else
                                <?php $count = 1; ?>
                                @foreach($datas as $data)
                                    @if($data->testimoni != null)
                                        <div class="swiper-slide">
                                            <blockquote class="blockquote">
                                                <div class="margin-b-20">
                                                    <p>{{ $data->testimoni }}</p>
                                                </div>
                                                <p>Dari: <span class="fweight-700 color-link">{{ $data->name }} </span> , {{ $data->tgl_testimoni }}</p>
                                            </blockquote>
                                        </div>
                                        @php
                                            $count++;
                                        @endphp
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="swiper-ulasanals-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Gallery -->
    <div id="gallery" class="about-section">        
        <div class="container">
            <div class="row marginBottom">
                <div class="col-sm-6">
                    <h2>Galeri</h2>
                </div>
            </div>
            <div class="row container">         
                    @if(count($photos) < 1)
                    <div class="text-center">
                        <h5>Belum ada foto untuk galeri</h5>
                    </div>
                    @else             
                        <?php $count = 1; ?>
                        @foreach ($photos as $photo)
                            @if($count == 6)
                                @break
                            @elseif($count==1)
                                <div class="masonry-grid-item col-xs-12 col-sm-6 col-md-6 marginBottom">
                                    <!-- Work -->
                                    <div class="work wow fadeInUp" data-wow-duration=".3" data-wow-delay=".1s">
                                        <a href="#" data-toggle="modal" data-target="#modal{{$photo->id_foto}}">
                                            <div class="work-overlay">
                                                <img class="full-width img-responsive" src="/storage/assets/{{ $coworking->id_user }}/{{$coworking->nama_co}}/foto/{{$photo->foto_co}}" alt="Foto dari {{ $coworking->nama_co }}">                     
                                            </div>   
                                        </a>    
                                    </div>
                                    <!-- End Work -->
                                </div>
                                @php
                                    $count++;
                                @endphp
                            @elseif($count==2)
                                <div class="masonry-grid-item col-xs-6 col-sm-6 col-md-6 marginBottom">
                                    <!-- Work -->
                                    <div class="work wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
                                        <a href="#" data-toggle="modal" data-target="#modal{{$photo->id_foto}}">
                                            <div class="work-overlay">
                                                <img class="full-width img-responsive" src="/storage/assets/{{ $coworking->id_user }}/{{$coworking->nama_co}}/foto/{{$photo->foto_co}}" alt="Foto dari {{ $coworking->nama_co }}">                     
                                            </div>   
                                        </a>  
                                    </div>
                                    <!-- End Work -->
                                </div>
                                @php
                                    $count++;
                                @endphp
                            @else
                                <div class="masonry-grid-item col-xs-6 col-sm-6 col-md-4 marginBottom">
                                    <!-- Work -->
                                    <div class="work wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
                                        <a href="#" data-toggle="modal" data-target="#modal{{$photo->id_foto}}">
                                            <div class="work-overlay">
                                                <img class="full-width img-responsive" src="/storage/assets/{{ $coworking->id_user }}/{{$coworking->nama_co}}/foto/{{$photo->foto_co}}" alt="Foto dari {{ $coworking->nama_co }}">                     
                                            </div>   
                                        </a>  
                                    </div>
                                    <!-- End Work -->
                                </div>
                                @php
                                    $count++;
                                @endphp
                            @endif
                        @endforeach
                    @endif
                    
                    {{ $photos->links() }}
            </div>
        </div>
    </div>

    <!-- Lokasi (alamat) -->
    <div id="lokasico" class="bg-white">
        <div class="section-seperator">
            <div class="container">
                <div class="row">
                    <!-- Contact List -->
                    <div class="col-sm-12">
                        <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                            <h3>{{ $coworking->alamat_co }}</h3>
                            <p>{{ $coworking->kota }}</p>
                            <hr>
                            <p>Jika ada yang ingin ditanyakan, dapat menghubungi detail kontak.</p>
                            <ul class="list-unstyled contact-list">
                                <li><span><i class="fa fa-phone color-base"></i> {{ $coworking->telp }}</span></li>
                                <li><span><i class="fa fa-envelope color-base"></i> {{ $coworking->email_co }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Google Maps -->
            <div>
                <div id="map-canvas" class="height-400">
                    <div class="text-center">
                        <h2>Cek koneksi atau refresh page agar dapat menampilkan maps</h2>
                    </div>
                </div>
            </div>
        </div>       
    </div>

    <!-- Kontak Kami -->
    <footer id="kontak" class="footer about-section" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url( {{ asset('assets/bg-dashboard.jpg')}}) no-repeat center;background-size: cover;">
        <!-- Links -->
        <div class="footer-seperator">
            <div class="content-lg container">
                <div class="row">
                    <div class="col-sm-2 sm-margin-b-50">
                        <!-- List -->
                        <ul class="list-unstyled footer-list">
                            <li class="footer-list-item"><a class="footer-list-link" href="/dashboard">Beranda</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="#deskripsi">Deskripsi</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="#harga-sewa">Harga Sewa</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="#fasilitasco">Fasilitas</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="#ulasan">Ulasan</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="#lokasico">Lokasi</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="#kontak">Kontak Kami</a></li>
                        </ul>
                        <!-- End List -->
                    </div>
                    <div class="col-sm-4">
                        <!-- List -->
                        <ul class="list-unstyled footer-list">
                            <li class="footer-list-item"><a class="footer-list-link" href="{{ $coworking->twitter }}" target="_blank">Twitter</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="{{ $coworking->fb }}" target="_blank">Facebook</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="{{ $coworking->ig }}" target="_blank">Instagram</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href="{{ $coworking->website }}" target="_blank">Website</a></li>
                        </ul>
                        <!-- End List -->
                    </div>
                    <div class="col-sm-5">
                        <h3 class="text-white">Kirim Pesan Untuk Kami</h3>
                        <form action="{{ url('coworking/profile/contact/'.$coworking->id_co) }}" method="POST">
                            @csrf
                            <input type="text" class="form-control footer-input margin-b-20" placeholder="Nama" value="{{ Auth::user()->name }}" name="kontak_nama" required>

                            <input type="email" class="form-control footer-input margin-b-20" placeholder="Email" value="{{ Auth::user()->email }}" name="kontak_email" required>

                            <input type="number" class="form-control footer-input margin-b-20" placeholder="Nomor Telepon" value="{{ Auth::user()->no_telp }}" name="kontak_telp" required>

                            <input type="text" class="form-control footer-input margin-b-20" placeholder="Subjek" value="{{ old('kontak_subjek') }}" name="kontak_subjek" required>
                            
                            <textarea class="form-control footer-input margin-b-30 marginBottom" rows="8" placeholder="Pesan" name="kontak_pesan" required>{{ old('kontak_pesan') }}</textarea>
                            
                            <button type="submit" class="btn btn-daftar marginBottom" role="button">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- Modal Coworking -->     
    <div class="modal fade" id="modal-coworking" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="Modal-label-1">Pemesanan Ruang Coworking</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <h5>Isi Data Pemesanan</h5>
                        <form action="{{ url('coworking/booking')}}" method="POST">
                            @csrf
                            <div class="form-group row hiddenForm">
                                <label for="id_user" class="col-md-3 col-form-label">id_user</label>        
                                <div class="col-md-9">
                                <input id="text" type="text" class="form-control" name="id_user" value="{{ Auth::user()->id}}" required>
                                </div>
                                <label for="id_co" class="col-md-3 col-form-label">id_co</label>        
                                <div class="col-md-9">
                                <input id="text" type="text" class="form-control" name="id_co" value="{{ $coworking->id_co }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label">Nama Lengkap</label>        
                                <div class="col-md-9">
                                    <input id="text" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{ Auth::user()->name }}" required>
                                    @if ($errors->has('nama'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('nama') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-3 col-form-label">Email</label>        
                                <div class="col-md-9">
                                    <input id="text" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email }}" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telp" class="col-md-3 col-form-label">Nomor Telepon</label>        
                                <div class="col-md-9">
                                    <input id="telp" type="number" class="form-control{{ $errors->has('telp') ? ' is-invalid' : '' }}" name="telp" value="{{ Auth::user()->no_telp }}" required>
                                    @if ($errors->has('telp'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('telp') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis_ruang" class="col-md-3 col-form-label">Jenis Ruang</label>        
                                <div class="col-md-9">
                                    <input id="text" type="text" class="form-control{{ $errors->has('jenis_ruang') ? ' is-invalid' : '' }}" name="jenis_ruang" value="Coworking" readonly="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis_waktu" class="col-md-3 col-form-label">Jenis Waktu Coworking</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="input-group mb-3">
                                            <select class="custom-select" id="jenis_waktu" name="jenis_waktu" required>
                                                <option selected>Pilih kenis waktu</option>
                                                <option value="Perhari">Perhari - Rp. {{ $coworking->coworking_perhari }},-</option>
                                                @if( $coworking->coworking_perbulan != null )
                                                <option value="Perbulan">Perbulan - Rp. {{ $coworking->coworking_perbulan }},-</option>
                                                @endif
                                                @if( $coworking->coworking_pertahun != null )
                                                <option value="Pertahun">Pertahun - Rp. {{ $coworking->coworking_pertahun }},-</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_sewa" class="col-md-3 col-form-label">Waktu Lama Sewa</label>        
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-6">
                                            <input id="text" type="number" class="form-control{{ $errors->has('waktu_sewa') ? ' is-invalid' : '' }}" name="waktu_sewa" placeholder="Waktu Lama Sewa" value="{{ old('waktu_sewa') }}"  min="1" max="31" required>
                                        </div>
                                        <div class="col-6">
                                            <label for="waktu_sewa" class="col-form-label">Hari/Bulan/Tahun</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-md-3 col-form-label">Tanggal Sewa</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input id="datepicker" type="date" class="form-control" name="tgl_sewa" required>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <select class="custom-select" id="banyak_orang" name="banyak_orang" required>
                                                    <option selected>Banyaknya Orang</option>
                                                    <option value="1">1</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pesan" class="col-md-3 col-form-label">Pesan</label>
                                <div class="col-md-9">
                                    <textarea class="form-control{{ $errors->has('pesan') ? ' is-invalid' : '' }}" rows="5" name="pesan" required>{{ old('pesan') }}</textarea>
                                    @if ($errors->has('pesan'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('pesan') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-12 col-md-10">
                                <div class="row">
                                    <div class="col-12">
                                        <h6>Catatan:</h6> <br/>
                                    </div>
                                    <div class="col-12">
                                        <p>Pembayaran langsung Coworking Space : {{ $coworking->bayar_ots }}</p>
                                    </div>
                                    <div class="col-12">
                                        <p>Pembayaran untuk {{ $coworking->nama_co }} dapat menggunakan Bank: <u>{{ $coworking->bank_utama }}</u> dengan nomor rekening: <u>{{ $coworking->rek_utama }}</u> atas nama: <u>{{ $coworking->nama_utama }}</u></p>
                                    </div>
                                    <div class="col-12">
                                        @if($coworking->bank_kedua == 'null')
                                            <p>Pembayaran untuk {{ $coworking->nama }} dapat menggunakan Bank: <u>{{ $coworking->bank_kedua }}</u> dengan nomor rekening: <u>{{ $coworking->rek_kedua }}</u> atas nama: <u>{{ $coworking->nama_kedua }}</u></p>
                                        @endif
                                    </div>
                                    <div class="col-12">
                                        <p>Jika sudah melakukan transfer pembayaran, dapat mengirimkan bukti pembayaran kepada email {{ $coworking->email }} dan sertakan nama yang melakukan pemesanan.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-2">
                                <button type="submit" class="btn btn-primary pull-right" role="button">Kirim</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Meeting -->        
    <div class="modal fade" id="modal-meeting" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="Modal-label-1">Pemesanan Sewa Ruang Meeting dan Diskusi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <h5>Isi Data Pemesanan</h5>
                        <form action="{{ url('coworking/booking')}}" method="POST">
                            @csrf
                            <div class="form-group row hiddenForm">
                                <label for="id_user" class="col-md-3 col-form-label">id</label>        
                                <div class="col-md-9">
                                    <input id="text" type="text" class="form-control" name="id_user" value="{{ Auth::user()->id}}" required>
                                    <input id="text" type="text" class="form-control" name="id_co" value="{{ $coworking->id_co }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label">Nama Lengkap</label>        
                                <div class="col-md-9">
                                    <input id="text" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{ Auth::user()->name }}" required>
                                    @if ($errors->has('nama'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('nama') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-3 col-form-label">Email</label>        
                                <div class="col-md-9">
                                    <input id="text" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email }}" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telp" class="col-md-3 col-form-label">Nomor Telepon</label>        
                                <div class="col-md-9">
                                    <input id="telp" type="number" class="form-control{{ $errors->has('telp') ? ' is-invalid' : '' }}" name="telp" value="{{ Auth::user()->no_telp }}" required>
                                    @if ($errors->has('telp'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('telp') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis_ruang" class="col-md-3 col-form-label">Jenis Ruang</label>        
                                <div class="col-md-9">
                                    <input id="text" type="text" class="form-control{{ $errors->has('jenis_ruang') ? ' is-invalid' : '' }}" name="jenis_ruang" value="Meeting" readonly="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis_waktu" class="col-md-3 col-form-label">Jenis Waktu Meeting</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="input-group mb-3">
                                            <select class="custom-select" id="jenis_waktu" name="jenis_waktu" required>
                                                <option selected>Pilih perjam, perhari, perbulan</option>
                                                @if( $coworking->meeting_perjam != null )
                                                <option value="Perjam">Perjam - Rp. {{ $coworking->meeting_perjam }},-</option>
                                                @endif
                                                @if( $coworking->meeting_perhari != null )
                                                <option value="Perhari">Perhari - Rp. {{ $coworking->meeting_perhari }},-</option>
                                                @endif
                                                @if( $coworking->meeting_perbulan != null )
                                                <option value="Perbulan">Perbulan - Rp. {{ $coworking->meeting_perbulan }},-</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_sewa" class="col-md-3 col-form-label">Waktu Lama Sewa</label>        
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-7">
                                            <input id="text" type="number" class="form-control{{ $errors->has('waktu_sewa') ? ' is-invalid' : '' }}" name="waktu_sewa" placeholder="Waktu Lama Sewa" value="{{ old('waktu_sewa') }}" min="1" max="31" required>
                                        </div>
                                        <div class="col-5">
                                            <label for="waktu_sewa" class="col-form-label">Jam/Hari/Bulan</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-md-3 col-form-label">Tanggal Sewa</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input id="datepicker" type="date" class="form-control" name="tgl_sewa" required>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <select class="custom-select" id="banyak_orang" name="banyak_orang" required>
                                                    <option selected>Banyaknya Orang</option>
                                                    @for($i=1; $i <= 10; $i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pesan" class="col-md-3 col-form-label">Pesan</label>
                                <div class="col-md-9">
                                    <textarea class="form-control{{ $errors->has('pesan') ? ' is-invalid' : '' }}" rows="5" name="pesan" required>{{ old('pesan') }}</textarea>
                                    @if ($errors->has('pesan'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('pesan') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-12 col-md-10">
                            <div class="row">
                                <div class="col-12">
                                    <h6>Catatan:</h6> <br/>
                                </div>
                                <div class="col-12">
                                    <p>Pembayaran langsung Coworking Space : {{ $coworking->bayar_ots }}</p>
                                </div>
                                <div class="col-12">
                                    <p>Pembayaran untuk {{ $coworking->nama_co }} dapat menggunakan Bank: <u>{{ $coworking->bank_utama }}</u> dengan nomor rekening: <u>{{ $coworking->rek_utama }}</u> atas nama: <u>{{ $coworking->nama_utama }}</u></p>
                                </div>
                                <div class="col-12">
                                    @if($coworking->bank_kedua == 'null')
                                        <p>Pembayaran untuk {{ $coworking->nama }} dapat menggunakan Bank: <u>{{ $coworking->bank_kedua }}</u> dengan nomor rekening: <u>{{ $coworking->rek_kedua }}</u> atas nama: <u>{{ $coworking->nama_kedua }}</u></p>
                                    @endif
                                </div>
                                <div class="col-12">
                                    <p>Jika sudah melakukan transfer pembayaran, dapat mengirimkan bukti pembayaran kepada email {{ $coworking->email }} dan sertakan nama yang melakukan pemesanan.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <button type="submit" class="btn btn-primary pull-right" role="button">Kirim</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Event -->        
    <div class="modal fade" id="modal-event" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="Modal-label-1">Pemesanan Sewa Event Space</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <h5>Isi Data Pemesanan</h5>
                        <form action="{{ url('coworking/booking')}}" method="POST">
                            @csrf
                            <div class="form-group row hiddenForm">
                                <label for="id_user" class="col-md-3 col-form-label">id</label>        
                                <div class="col-md-9">
                                <input id="text" type="text" class="form-control" name="id_user" value="{{ Auth::user()->id}}" required>
                                <input id="text" type="text" class="form-control" name="id_co" value="{{ $coworking->id_co }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label">Nama Lengkap</label>        
                                <div class="col-md-9">
                                    <input id="text" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{ Auth::user()->name }}" required>
                                    @if ($errors->has('nama'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('nama') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-3 col-form-label">Email</label>        
                                <div class="col-md-9">
                                    <input id="text" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email }}" required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="telp" class="col-md-3 col-form-label">Nomor Telepon</label>        
                                <div class="col-md-9">
                                    <input id="telp" type="number" class="form-control{{ $errors->has('telp') ? ' is-invalid' : '' }}" name="telp" value="{{ Auth::user()->no_telp }}" required>
                                    @if ($errors->has('telp'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('telp') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis_ruang" class="col-md-3 col-form-label">Jenis Ruang</label>        
                                <div class="col-md-9">
                                    <input id="text" type="text" class="form-control{{ $errors->has('jenis_ruang') ? ' is-invalid' : '' }}" name="jenis_ruang" value="Event" readonly="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis_waktu" class="col-md-3 col-form-label">Jenis Waktu Sewa</label>        
                                <div class="col-md-9">
                                    <input id="text" type="text" class="form-control{{ $errors->has('jenis_waktu') ? ' is-invalid' : '' }}" name="jenis_waktu" value="Perjam" readonly="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_sewa" class="col-md-3 col-form-label">Waktu Lama Sewa</label>        
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-9">
                                            <input id="text" type="number" class="form-control{{ $errors->has('waktu_sewa') ? ' is-invalid' : '' }}" name="waktu_sewa" placeholder="Waktu Lama Sewa" min="1" max="24" required>
                                        </div>
                                        <div class="col-3">
                                            <label for="waktu_sewa" class="col-form-label">Jam</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-md-3 col-form-label">Tanggal Sewa</label>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input id="datepicker" type="date" class="form-control" name="tgl_sewa" required>                    
                                        </div>
                                        <div class="col-sm-6">
                                            <input id="text" type="number" class="form-control{{ $errors->has('banyak_orang') ? ' is-invalid' : '' }}" name="banyak_orang" placeholder="Banyak Orang"  min="1" max="300" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pesan" class="col-md-3 col-form-label">Pesan</label>
                                <div class="col-md-9">
                                    <textarea class="form-control{{ $errors->has('pesan') ? ' is-invalid' : '' }}" rows="5" name="pesan" required>{{ old('pesan') }}</textarea>
                                    @if ($errors->has('pesan'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('pesan') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-12 col-md-10">
                            <div class="row">
                                <div class="col-12">
                                    <h6>Catatan:</h6> <br/>
                                </div>
                                <div class="col-12">
                                    <p>Pembayaran langsung Coworking Space : {{ $coworking->bayar_ots }}</p>
                                </div>
                                <div class="col-12">
                                    <p>Pembayaran untuk {{ $coworking->nama_co }} dapat menggunakan Bank: <u>{{ $coworking->bank_utama }}</u> dengan nomor rekening: <u>{{ $coworking->rek_utama }}</u> atas nama: <u>{{ $coworking->nama_utama }}</u></p>
                                </div>
                                <div class="col-12">
                                    @if($coworking->bank_kedua == 'null')
                                        <p>Pembayaran untuk {{ $coworking->nama }} dapat menggunakan Bank: <u>{{ $coworking->bank_kedua }}</u> dengan nomor rekening: <u>{{ $coworking->rek_kedua }}</u> atas nama: <u>{{ $coworking->nama_kedua }}</u></p>
                                    @endif
                                </div>
                                <div class="col-12">
                                    <p>Jika sudah melakukan transfer pembayaran, dapat mengirimkan bukti pembayaran kepada email {{ $coworking->email }} dan sertakan nama yang melakukan pemesanan.<p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <button type="submit" class="btn btn-primary pull-right" role="button">Kirim</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Gambar --> 
    @foreach($photos as $photo)       
    <div class="modal fade" id="modal{{$photo->id_foto}}">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="Modal-label-1">Foto {{$coworking->nama_co}} </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <img class="img-thumbnail img-fluid center" src="/storage/assets/{{ $coworking->id_user }}/{{$coworking->nama_co}}/foto/{{$photo->foto_co}}" alt="Foto dari {{ $coworking->nama_co }}"/>
                    </div>
                </div>
                <div class="modal-footer">            
                    @if(Auth::user()->id == $coworking->id_user)
                        <form id="delete-photo" method="post" action="{{ url('coworking/profile/'.$coworking->id_co, $photo->id_foto) }}">
                            <input type="hidden" name="_method" value="delete" />
                            @csrf
                        </form>
                        <a class="btn btn-primary" href="#" onclick="event.preventDefault(); document.getElementById('delete-photo').submit();">
                            <h6>Hapus Foto</h6>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endforeach

    <!-- Modal Testimoni -->        
    <div class="modal fade" id="modal-testi" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="Modal-label-1">Testimoni {{$coworking->nama_co}} </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        @if(count($dataulasan) < 1)
                            <div class="text-center">
                                <h5>Belum ada ulasan untuk coworking ini</h5>
                            </div>
                        @else
                            @foreach($datas as $data)
                                @if($data->testimoni != null)
                                    <div class="card marginBottom">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    @if($data->foto_profil == null)
                                                        <img class="img-thumbnail img-fluid" src="{{ asset('assets/noimage.png')}}" alt="photo">
                                                    @else
                                                        <img class="img-thumbnail img-fluid" src="/storage/assets/{{ $data->id }}/foto_profil/{{ $data->foto_profil }}" alt="Foto User">
                                                    @endif
                                                </div>
                                                <div class="col-sm-9">
                                                    <h5>{{ $data->name }}, {{ $data->tgl_testimoni }}</h5>
                                                    <blockquote class="blockquote">
                                                        <p>{{ $data->testimoni }}</p>
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        document.getElementById('datepicker').value = Date();
        var marker;
    
        function initMap() {        
            var coworking = @json($coworking);
            var lat_co = parseFloat(coworking.lat);
            var lng_co = parseFloat(coworking.lng);

            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 17,
                center: {lat: lat_co, lng: lng_co}
            });
    
            marker = new google.maps.Marker({
                map: map,
                animation: google.maps.Animation.DROP,
                position: {lat: lat_co, lng: lng_co}
            });
            marker.addListener('click', toggleBounce);
        }
    
        function toggleBounce() {
            if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
            } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyAGNSrdEm76jTm6T-BVmGcsfBgIhJRq4&callback=initMap"></script>
    <!-- CORE PLUGINS -->
    <script src="{{ asset('jquery/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('jquery/jquery1.11.0-ui.js') }}"></script>

    <!-- PAGE LEVEL PLUGINS -->
    <script src="{{ asset('jquery-easing/jquery.easing.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('jquery/jquery.wow.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/swiper/js/swiper.jquery.min.js') }}" type="text/javascript"></script>

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('js/components/wow.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/components/swiper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@endsection