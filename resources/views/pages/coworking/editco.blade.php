@extends('layouts.master')

@section('title')
    Ubah Profil {{ $coworking->nama_co }}
@endsection

@section('header')
    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/swiper/css/swiper.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- THEME STYLES -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    @include('partials.navbar.navbar')
    @php
        $namaco = str_replace(' ', '%20', $coworking->nama_co);
    @endphp
    <!-- Header -->
    <header id="coworking-header" class="header-wrapper home-parallax home-fade random-header" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url({{ URL::asset('/storage/assets/'.$coworking->id_user.'/'.$namaco.'/header/'.$coworking->header_co) }}) no-repeat center;background-size: cover; background-attachment: fixed;">
        <div class="header-overlay"></div>
        <div class="header-wrapper-inner">
            <div class="container">
                <div class="welcome-speech">
                    <h1>Perbaharui data</h1>
                    <a href="{{ url('coworking/profile/'.$coworking->id_co) }}"><p class="text-uppercase"><u>{{ $coworking->nama_co }}</u></p></a>
                    @include('partials.messages')
                </div>
            </div>
        </div>
    </header>

    <div id="thumbnails" class="addco marginBottom" data-auto-height="true">
        <div class="container">
                <div class="about-choose">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="row">
                                <!-- Informasi Coworking -->
                                <div class="col-sm-12">
                                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                                        <div class="service" data-height="height" class="informasi">
                                            <div class="service-info">
                                                <h5>Informasi</h5>
                                            </div>
                                            <a href="#informasi" class="content-wrapper-link"></a>    
                                        </div>
                                    </div>
                                </div>
                                <!-- Komunikasi Coworking -->
                                <div class="col-sm-12">
                                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                                        <div class="service" data-height="height" class="komunikasi">
                                            <div class="service-info">
                                                <h5>Komunikasi</h5>
                                            </div>
                                            <a href="#komunikasi" class="content-wrapper-link"></a>    
                                        </div>
                                    </div>
                                </div>
                                <!-- Lokasi Coworking -->
                                <div class="col-sm-12">
                                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                                        <div class="service" data-height="height" class="lokasi">
                                            <div class="service-info">
                                                <h5>Lokasi</h5>
                                            </div>
                                            <a href="#lokasi" class="content-wrapper-link"></a>    
                                        </div>
                                    </div>
                                </div>
                                <!-- Fasilitas Coworking -->
                                <div class="col-sm-12">
                                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                                        <div class="service" data-height="height" class="fasilitas">
                                            <div class="service-info">
                                                <h5>Fasilitas</h5>
                                            </div>
                                            <a href="#fasilitas" class="content-wrapper-link"></a>    
                                        </div>
                                    </div>
                                </div>
                                <!-- Foto Coworking -->
                                <div class="col-sm-12">
                                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                                        <div class="service" data-height="height" class="foto">
                                            <div class="service-info">
                                                <h5>Foto</h5>
                                            </div>
                                            <a href="#foto" class="content-wrapper-link"></a>    
                                        </div>
                                    </div>
                                </div>
                                <!-- Pembayaran Coworking -->
                                <div class="col-sm-12">
                                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                                        <div class="service" data-height="height" class="pembayaran">
                                            <div class="service-info">
                                                <h5>Pembayaran</h5>
                                            </div>
                                            <a href="#pembayaran" class="content-wrapper-link pembayaran"></a>    
                                        </div>
                                    </div>
                                </div>
                                <!-- Harga Coworking -->
                                <div class="col-sm-12">
                                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                                        <div class="service" data-height="height" class="harga">
                                            <div class="service-info">
                                                <h5>Harga Sewa</h5>
                                            </div>
                                            <a href="#harga" class="content-wrapper-link harga"></a>    
                                        </div>
                                    </div>
                                </div>
                                <!-- Simpan Coworking -->
                                <div class="col-sm-12 margin-b-2">
                                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                                        <div class="service" data-height="height" class="simpan">
                                            <div class="service-info">
                                                <h5>Simpan Data</h5>
                                            </div>
                                            <a href="#simpan" class="content-wrapper-link"></a>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <div class="about-choose container">
                                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                                    <div class="service">
                                        <form action="{{ url('coworking/profile/'.$coworking->id_co)}}" method="POST" enctype="multipart/form-data" file="true">
                                            <input type="hidden" name="_method" value="put">
                                            @csrf
                                            <!-- Opening -->
                                            <div id="opening" class="form-content-choose-opening">
                                                <h4>Selamat datang di Fimespace</h4>
                                                <p class="text-muted marginBottom">Anda berada dalam form untuk mengubah data-data coworking space, agar anda mengetahui ketentuan yang berlaku didalam Fimespace, anda dapat ke tab Simpan Data</p>
                                                <div class="konten">
                                                    <h6 class="text-muted text-center">Isi data sesuai ketentuan dan hal-hal yang harus disampaikan didalam Fimespace, selamat mengisi.</h6>
                                                </div>
                                            </div>
                                            <!-- Informasi Coworking -->
                                            <div id="informasi" class="form-content-choose">
                                                <h5>Informasi</h5>
                                                <p class="text-muted marginBottom">Informasikan mengenai coworking space-mu, deskripsikan konsep serta suasana.</p>
                                                <div class="konten">
                                                    <div class="form-group row">
                                                        <label for="nama_coworking" class="col-md-3 col-form-label">Nama Coworking</label>
                                                        <div class="col-md-9">
                                                            <input id="nama_coworking" type="text" class="form-control{{ $errors->has('nama_coworking') ? ' is-invalid' : '' }}" name="nama_coworking" placeholder="Nama Coworking Space" value="{{ $coworking->nama_co }}" required>
                                                            @if ($errors->has('nama_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('nama_coworking') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="desc_coworking" class="col-md-3 col-form-label">Deskripsi Coworking</label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control{{ $errors->has('desc_coworking') ? ' is-invalid' : '' }}" rows="5" name="desc_coworking" placeholder="deskripsikan coworking min.255 kata" required>{{ $coworking->desc }}</textarea>
                                                            @if ($errors->has('desc_coworking'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('desc_coworking') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="website_coworking" class="col-md-3 col-form-label">Website URL</label>
                                                        <div class="col-md-9">
                                                            <input id="website_coworking" type="text" class="form-control{{ $errors->has('website_coworking') ? ' is-invalid' : '' }}" name="website_coworking" placeholder="Contoh: http://fimespace.com" value="{{ $coworking->website }}">
                                                            @if ($errors->has('website_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('website_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <a class="btn btn-primary pull-right" role="button" href="#komunikasi"><b>Selanjutnya</b></a>
                                                </div>
                                            </div>
                                            <!-- Komunikasi Coworking -->
                                            <div id="komunikasi" class="form-content-choose">
                                                <h5>Komunikasi</h5>
                                                <p class="text-muted marginBottom">Infokan bagaimana calon coworkers dapat berkomunikasi dengan admin ataupun coworking spacenya</p>
                                                <div class="konten">
                                                    <div class="form-group row">
                                                        <label for="email_coworking" class="col-md-3 col-form-label">Email Utama</label>
                                                        <div class="col-md-9">
                                                            <input id="email_coworking" type="email" class="form-control{{ $errors->has('email_coworking') ? ' is-invalid' : '' }}" name="email_coworking" placeholder="Contoh: fimespace@gmail.com" value="{{ $coworking->email_co }}" required>
                                                            @if ($errors->has('email_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('email_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="telp_coworking" class="col-md-3 col-form-label">Telpon</label>
                                                        <div class="col-md-9">
                                                            <input id="telp_coworking" type="number" class="form-control{{ $errors->has('telp_coworking') ? ' is-invalid' : '' }}" name="telp_coworking" value="{{ $coworking->telp }}" placeholder="Contoh: 0218123456"  required>
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('telp_coworking') }}</strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group row">
                                                        <label for="fb_coworking" class="col-md-3 col-form-label">Facebook Page</label>
                                                        <div class="col-md-9">
                                                            <input id="fb_coworking" type="text" class="form-control{{ $errors->has('fb_coworking') ? ' is-invalid' : '' }}" name="fb_coworking" placeholder="Contoh: https://www.facebook.com/fimespace/" value="{{ $coworking->fb }}">
                                                            @if ($errors->has('fb_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('fb_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="twitter_coworking" class="col-md-3 col-form-label">Twitter</label>
                                                        <div class="col-md-9">
                                                            <input id="twitter_coworking" type="text" class="form-control{{ $errors->has('twitter_coworking') ? ' is-invalid' : '' }}" name="twitter_coworking" placeholder="Contoh: https://www.twitter.com/fimespace/" value="{{ $coworking->twitter }}">
                                                            @if ($errors->has('twitter_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('twitter_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="ig_coworking" class="col-md-3 col-form-label">Instagram</label>
                                                        <div class="col-md-9">
                                                            <input id="ig_coworking" type="text" class="form-control{{ $errors->has('ig_coworking') ? ' is-invalid' : '' }}" name="ig_coworking" placeholder="Contoh: https://www.instagram.com/fimespace/" value="{{ $coworking->ig }}">
                                                            @if ($errors->has('ig_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('ig_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary pull-right" role="button" href="#lokasi"><b>Selanjutnya</b></a>                                       
                                            </div>
                                            <!-- Lokasi Coworking -->
                                            <div id="lokasi" class="form-content-choose">
                                                <h5>Lokasi</h5>
                                                <p class="text-muted marginBottom">Lokasikan dengan tepat alamat coworking spacemu.</p>
                                                <div class="konten">
                                                    <div class="form-group row">
                                                        <label for="kota_coworking" class="col-md-3 col-form-label">Kota</label>
                                                        <div class="col-md-9">
                                                            <input id="kota_coworking" type="text" class="form-control{{ $errors->has('kota_coworking') ? ' is-invalid' : '' }}" name="kota_coworking" value="{{ $coworking->kota }}" placeholder="Cari kota..." required>
                                                            @if ($errors->has('kota_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('kota_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="alamat_coworking" class="col-md-3 col-form-label">Alamat Coworking</label>
                                                        <div class="col-md-9">
                                                            <input id="searchmap" name="alamat_coworking" type="text" class="form-control{{ $errors->has('alamat_coworking') ? ' is-invalid' : '' }}" name="alamat_coworking" value="{{ $coworking->alamat_co }}" placeholder="Cari Lokasi.." required>
                                                            @if ($errors->has('alamat_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('alamat_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                            <div id="map-canvas">map</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row hiddenForm">
                                                        <label for="alamat_coworking" class="col-md-3 col-form-label"></label>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input id="lng" type="text" class="form-control{{ $errors->has('lng') ? ' is-invalid' : '' }}" name="lng" value="{{ $coworking->lng }}" required>
                                                                @if ($errors->has('lng'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('lng') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                    
                                                            <div class="col-md-6">
                                                                <input id="lat" type="text"  class="form-control{{ $errors->has('lat') ? ' is-invalid' : '' }}" name="lat" value="{{ $coworking->lat }}"  required>
                                                                @if ($errors->has('lat'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('lat') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary pull-right" role="button" href="#fasilitas"><b>Selanjutnya</b></a> 
                                            </div>
                                            <!-- Fasilitas Coworking -->
                                            <div id="fasilitas" class="form-content-choose">
                                                <h5>Fasilitas</h5>
                                                <p class="text-muted marginBottom">Masukkan fasilitas-fasilitas yang disediakan oleh coworking space.</p>
                                                <div class="konten">
                                                    <div class="form-group row">
                                                        <label for="fasilitas_coworking" class="col-md-3 col-form-label">Fasilitas</label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control{{ $errors->has('fasilitas_coworking') ? ' is-invalid' : '' }}" rows="5" id="article-ckeditor" name="fasilitas_coworking" required>{!! $coworking->fasilitas !!}</textarea>
                                                            @if ($errors->has('fasilitas_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('fasilitas_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary pull-right" role="button" href="#foto"><b>Selanjutnya</b></a> 
                                            </div>
                                            <!-- Foto Coworking -->
                                            <div id="foto" class="form-content-choose">
                                                <h5>Foto</h5>
                                                <p class="text-muted marginBottom">Masukkan logo dan header coworking space. <br><br>Disarankan untuk header coworking space menggunakan ukuran 1920x597px.</p>
                                                <div class="konten">
                                                    <div class="form-group row">
                                                        <label for="logo_coworking" class="col-md-3 col-form-label">Upload Logo</label>
                                                        <div class="col-md-9">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" id="customFile" value="{{ $coworking->logo_co }}" name="logo_coworking">
                                                                <label class="custom-file-label" for="customFile">{{ $coworking->logo_co }}</label>
                                                                @if ($errors->has('logo_coworking'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('logo_coworking') }}</strong>
                                                                    </span>
                                                                @endif  
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="header_coworking" class="col-md-3 col-form-label">Upload Header</label>
                                                        <div class="col-md-9">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" id="customFile" value="{{ $coworking->header_co }}" name="header_coworking">
                                                                <label class="custom-file-label" for="customFile">{{ $coworking->header_co }}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="image_coworking" class="col-md-3 col-form-label">Foto Coworking</label>
                                                        <div class="col-md-9">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" id="customFile" name="file[]" multiple="true">
                                                                <label class="custom-file-label" for="customFile">Upload Photo</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary pull-right" role="button" href="#pembayaran"><b>Selanjutnya</b></a> 
                                            </div>
                                            <!-- Pembayaran Coworking -->
                                            <div id="pembayaran" class="form-content-choose">
                                                <h5>Pembayaran</h5>
                                                <p class="text-muted marginBottom">Masukkan bank dan nomor rekening utama. Rekening kedua merupakan opsional jika coworking space mempunyai dua rekening</p>
                                                <div class="konten">
                                                    <div class="form-group row">
                                                        <label for="nama_utama_coworking" class="col-md-3 col-form-label">Atas Nama Utama</label>
                                                        <div class="col-md-9">
                                                            <input id="nama_utama_coworking" type="text" class="form-control{{ $errors->has('nama_utama_coworking') ? ' is-invalid' : '' }}" name="nama_utama_coworking" placeholder="Atas Nama Pemilik Rekening Utama" value="{{ $coworking->nama_utama }}" required>
                                                            @if ($errors->has('nama_utama_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('nama_utama_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="bank_utama_coworking" class="col-md-3 col-form-label">Bank Utama</label>
                                                        <div class="col-md-9">
                                                            <input id="bank_utama_coworking" type="text" class="form-control{{ $errors->has('bank_utama_coworking') ? ' is-invalid' : '' }}" name="bank_utama_coworking" placeholder="Contoh: Bca, Mandiri, dll" value="{{ $coworking->bank_utama }}" required>
                                                            @if ($errors->has('bank_utama_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('bank_utama_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="rek_utama_coworking" class="col-md-3 col-form-label">Nomor Rekening Utama</label>
                                                        <div class="col-md-9">
                                                            <input id="rek_utama_coworking" type="text" class="form-control{{ $errors->has('rek_utama_coworking') ? ' is-invalid' : '' }}" name="rek_utama_coworking" placeholder="Tulis Nomor Rekening Utama" value="{{$coworking->rek_utama }}" required>
                                                            @if ($errors->has('rek_utama_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('rek_utama_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group row">
                                                        <label for="nama_kedua_coworking" class="col-md-3 col-form-label">Atas Nama Kedua</label>
                                                        <div class="col-md-9">
                                                            <input id="nama_kedua_coworking" type="text" class="form-control{{ $errors->has('nama_kedua_coworking') ? ' is-invalid' : '' }}" name="nama_kedua_coworking" placeholder="Atas Nama Pemilik Rekening Kedua" value="{{ $coworking->nama_kedua }}">
                                                            @if ($errors->has('nama_kedua_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('nama_kedua_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="bank_kedua_coworking" class="col-md-3 col-form-label">Bank Kedua</label>
                                                        <div class="col-md-9">
                                                            <input id="bank_kedua_coworking" type="text" class="form-control{{ $errors->has('bank_kedua_coworking') ? ' is-invalid' : '' }}" name="bank_kedua_coworking" placeholder="Contoh: Bca, Mandiri, dll" value="{{ $coworking->bank_kedua }}">
                                                            @if ($errors->has('bank_kedua_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('bank_kedua_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="rek_kedua_coworking" class="col-md-3 col-form-label">Nomor Rekening Kedua</label>
                                                        <div class="col-md-9">
                                                            <input id="rek_kedua_coworking" type="number" class="form-control{{ $errors->has('rek_kedua_coworking') ? ' is-invalid' : '' }}" name="rek_kedua_coworking" placeholder="Tulis Nomor Rekening Kedua" value="{{$coworking->rek_kedua }}">                                                        
                                                            @if ($errors->has('rek_kedua_coworking'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('rek_kedua_coworking') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group row">
                                                        <label for="bayar_ots" class="col-md-3 col-form-label">Bayar Ditempat</label>
                                                        <div class="col-md-9">
                                                            <select class="custom-select" id="bayar_ots" name="bayar_ots" value="bayar_ots" required>
                                                                <option selected>{{ $coworking->bayar_ots }}</option>
                                                                <option value="Ya">Ya</option>
                                                                <option value="Tidak">Tidak</option>
                                                            </select>
                                                            @if ($errors->has('bayar_ots'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('bayar_ots') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary pull-right" role="button" href="#harga"><b>Selanjutnya</b></a> 
                                            </div>
                                            <!-- Harga Coworking -->
                                            <div id="harga" class="form-content-choose">
                                                <h5>Harga Sewa</h5>
                                                <p class="text-muted marginBottom">Masukan harga sewa dalam coworking anda</p>
                                                <div class="konten">
                                                    <!-- Coworking Room -->
                                                    <p class="text-center">Ruang Coworking</p>
                                                    <div class="form-group row">
                                                        <label for="harga_co_desc" class="col-md-3 col-form-label">Catatan Ruang Coworking</label>
                                                        <div class="col-md-9">                                                       
                                                            <textarea class="form-control{{ $errors->has('harga_co_desc') ? ' is-invalid' : '' }}" rows="5" name="harga_co_desc">{{ $coworking->coworking_desc }}</textarea>
                                                            @if ($errors->has('harga_co_desc'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_co_desc') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="harga_perhari" class="col-md-3 col-form-label">Harga Coworking Perhari</label>
                                                        <div class="col-md-9">
                                                            <div class="row col-sm-12">
                                                                <label for="rupiah" class="col-md-2 col-form-label">Rp.</label>
                                                                <input id="harga_perhari" type="number" class="form-control{{ $errors->has('harga_perhari') ? ' is-invalid' : '' }} col-sm-9" min="1" name="harga_perhari" placeholder="Contoh: 100.000" value="{{ $coworking->coworking_perhari }}">
                                                                @if ($errors->has('harga_perhari'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('harga_perhari') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="harga_perbulan" class="col-md-3 col-form-label">Harga Coworking Perbulan</label>
                                                        <div class="col-md-9">
                                                            <div class="row col-sm-12">
                                                                <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                                <input id="harga_perbulan" type="number" class="form-control{{ $errors->has('harga_perbulan') ? ' is-invalid' : '' }} col-sm-9" min="1" name="harga_perbulan" placeholder="Contoh: 100.000" value="{{ $coworking->coworking_perbulan }}">
                                                                @if ($errors->has('harga_perbulan'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('harga_perbulan') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="harga_pertahun" class="col-md-3 col-form-label">Harga Coworking Pertahun</label>
                                                        <div class="col-md-9">
                                                            <div class="row col-sm-12">
                                                                <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                                <input id="harga_pertahun" type="number" class="form-control{{ $errors->has('harga_pertahun') ? ' is-invalid' : '' }} col-sm-9" min="1" name="harga_pertahun" placeholder="Contoh: 100.000" value="{{ $coworking->coworking_pertahun }}">
                                                                @if ($errors->has('harga_pertahun'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('harga_pertahun') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <p class="text-center">Ruang Meeting atau Diskusi</p>
                                                    <!-- Meeting Room -->
                                                    <div class="form-group row">
                                                        <label for="harga_meeting_desc" class="col-md-3 col-form-label">Catatan Ruang Meeting</label>
                                                        <div class="col-md-9">                                                                                                       
                                                            <textarea class="form-control{{ $errors->has('harga_meeting_desc') ? ' is-invalid' : '' }}" rows="5" name="harga_meeting_desc" value="{{ old('harga_meeting_desc') }}">{{ $coworking->meeting_desc }}</textarea>
                                                            @if ($errors->has('harga_meeting_desc'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_meeting_desc') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="harga_meeting_perjam" class="col-md-3 col-form-label">Harga Ruang Meeting Perjam</label>
                                                        <div class="col-md-9">
                                                            <div class="row col-sm-12">
                                                                <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                                <input id="harga_meeting_perjam" type="number" class="form-control{{ $errors->has('harga_meeting_perjam') ? ' is-invalid' : '' }} col-sm-10" min="1" name="harga_meeting_perjam" placeholder="Contoh: 100.000" value="{{ $coworking->meeting_perjam }}">
                                                                @if ($errors->has('harga_meeting_perjam'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('harga_meeting_perjam') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="harga_meeting_perhari" class="col-md-3 col-form-label">Harga Ruang Meeting Perhari</label>
                                                        <div class="col-md-9">
                                                            <div class="row col-sm-12">
                                                                <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                                <input id="harga_meeting_perhari" type="number" class="form-control{{ $errors->has('harga_meeting_perhari') ? ' is-invalid' : '' }} col-sm-10" min="1" name="harga_meeting_perhari" placeholder="Contoh: 100.000" value="{{ $coworking->meeting_perhari }}">
                                                                @if ($errors->has('harga_meeting_perhari'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('harga_meeting_perhari') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="harga_meeting_perbulan" class="col-md-3 col-form-label">Harga Ruang Meeting Perbulan</label>
                                                        <div class="col-md-9">
                                                            <div class="row col-sm-12">
                                                                <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                                <input id="harga_meeting_perbulan" type="number" class="form-control{{ $errors->has('harga_meeting_perbulan') ? ' is-invalid' : '' }} col-sm-10" min="1" name="harga_meeting_perbulan" placeholder="Contoh: 100.000" value="{{ $coworking->meeting_perbulan }}">
                                                                @if ($errors->has('harga_meeting_perbulan'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('harga_meeting_perbulan') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>                                                
                                                    <p class="text-center">Event Space</p>
                                                    <!-- Event Room -->
                                                    <div class="form-group row">
                                                        <label for="harga_event_desc" class="col-md-3 col-form-label">Catatan Event Space</label>
                                                        <div class="col-md-9">                                                        
                                                            <textarea class="form-control{{ $errors->has('harga_event_desc') ? ' is-invalid' : '' }}" id="article-ckeditor2"  rows="5" name="harga_event_desc">{{ $coworking->event_desc }}</textarea>
                                                            @if ($errors->has('harga_event_desc'))
                                                                <span class="invalid-feedback">
                                                                    <strong>{{ $errors->first('harga_event_desc') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="harga_event" class="col-md-3 col-form-label">Harga Event Space</label>
                                                        <div class="col-md-9">
                                                            <div class="row col-sm-12">
                                                                <label for="rupiah" class="col-sm-2 col-form-label">Rp.</label>
                                                                <input id="harga_event" type="number" class="form-control{{ $errors->has('harga_event') ? ' is-invalid' : '' }} col-sm-10" name="harga_event" min="1" placeholder="Contoh: 100.000" value="{{ $coworking->event_harga }}">
                                                                @if ($errors->has('harga_event'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('harga_event') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="btn btn-primary pull-right" role="button" href="#simpan"><b>Simpan Data</b></a> 
                                            </div>
                                            <!-- Submit Coworking -->
                                            <div id="simpan" class="form-content-choose">
                                            <h5>Simpan Data Coworking Space</h5>
                                            <div class="card marginBottom">
                                                <div class="card-body">
                                                    <p class="text-muted"><u>Persyaratan dan Ketentuan</u></p>
                                                    Ruang Coworking dapat memberi coworkes akses ke ruang kantor, workstation, akses Internet, peralatan kantor, ruang konferensi, sumber daya pengetahuan, dan fasilitas lainnya. <br>
                                                    Anda tidak akan menggunakan Layanan pada Fimespace untuk tujuan apa pun yang melanggar hukum atau dilarang oleh syarat, ketentuan, dan pemberitahuan ini. Anda tidak boleh menggunakan Layanan dengan cara apa pun yang dapat merusak, melumpuhkan, membebani secara berlebihan, atau melakukan penipuan terhadap pengguna dari Fimespace, serta mengganggu penggunaan dan kenikmatan pihak lain dari Layanan apa pun. <br>
                                                    Anda setuju bahwa ketika berpartisipasi atau menggunakan Layanan, Anda tidak akan:
                                                    <ul id="termncon">
                                                        <li>Mempublikasikan, mengeposkan, mengunggah, mendistribusikan atau menyebarluaskan topik, nama, materi atau informasi yang tidak pantas, tidak senonoh, memfitnah, tidak senonoh, tidak senonoh atau melanggar hukum pada atau melalui server Ruang Coworking.</li>
                                                        <li>Mencemarkan nama baik, menyalahgunakan, melecehkan, mengintai, mengancam atau melanggar hak hukum (seperti hak privasi dan publisitas) orang lain;</li>
                                                        <li>Gunakan materi atau informasi apa pun, termasuk gambar atau foto, yang disediakan melalui layanan dengan cara apa pun yang melanggar hak cipta, merek dagang, paten, rahasia dagang, atau hak kepemilikan lainnya dari pihak mana pun;</li>
                                                        <li>Batasi atau hambat pengguna lain untuk menggunakan dan menikmati Layanan</li>
                                                        <li>Buat identitas palsu untuk tujuan menyesatkan orang lain.</li>
                                                    </ul>
                                                    Jika terjadi pelanggaran syarat dan ketentuan dari bagian ini, partisipasi Anda di Fimespacea akan diakhiri, segera.<br>
                                                </div>
                                            </div>
                                            
                                                <hr>
                                                <div class="konten">
                                                    <button type="submit" class="btn btn-primary pull-right" role="button">Simpan Data</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>  
@endsection

@section('footer')
    <script>
        $(document).ready(function(){
            $("#informasi,#komunikasi,#lokasi,#fasilitas,#foto,#pembayaran,#harga,#simpan").hide();
            $(".service a").click(function(){
                hrefservice = $(this).attr('href');
                console.log(hrefservice);

                $('.form-content-choose').each(function(){
                    var ids = '#'+$(this).attr('id');
                    if(ids != hrefservice){
                        $(this).hide();
                    }else{
                        $(this).show();
                        $("#opening").hide();
                        $(this).removeClass("hidden");
                    }
                });

            });
        });

        var map, myLatLang, marker;
        var coworking = @json($coworking);
        function initialize(){
            success();
            activatePlacesSearch();
        }
    
        function success(){
            var latval = coworking.lat;
            var lngval = coworking.lng;
            myLatLang = new google.maps.LatLng(latval, lngval);
            $('#lat').val(latval);
            $('#lng').val(lngval);
    
            createMap(myLatLang);
            searchmaps();
        }
      
        function createMap(myLatLang){
            map =  new google.maps.Map(document.getElementById('map-canvas'),{
                center:myLatLang,
                zoom:17
            });
            marker =  new google.maps.Marker({
                position: myLatLang,
                map: map,
                draggable:true
            });
        }
    
        function searchmaps(){
            var searchBox= new google.maps.places.SearchBox(document.getElementById('searchmap'));
    
            google.maps.event.addListener(searchBox,'places_changed', function(){
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
    
                for(i=0; place=places[i];i++){
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }
                map.fitBounds(bounds);
                map.setZoom(13);
            });
    
            google.maps.event.addListener(marker, 'position_changed',function(){
                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();
    
                $('#lat').val(lat);
                $('#lng').val(lng);
            })
        }
    
        function activatePlacesSearch(){
            var input = document.getElementById('kota_coworking');
            var autocomplete = new google.maps.places.Autocomplete(input);
        }
    </script> 
    
    <!-- API Location -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyAGNSrdEm76jTm6T-BVmGcsfBgIhJRq4&language=id&libraries=places&callback=initialize"></script>
    </script>
    <!-- CORE PLUGINS -->
    <script src="{{ asset('jquery/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('jquery/jquery1.11.0-ui.js') }}"></script>

    <!-- PAGE LEVEL PLUGINS -->
    <script src="{{ asset('jquery-easing/jquery.easing.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('jquery/jquery.wow.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/swiper/js/swiper.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('js/components/wow.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/components/swiper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script> 
@endsection