@extends('layouts.master')

@section('title')
    Daftar Pemesanan {{ $coworking->nama_co }}
@endsection

@section('header')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
@endsection

@section('content')
    @include('partials.navbar.navbar')
    @php
        $namaco = str_replace(' ', '%20', $coworking->nama_co);
    @endphp
    <!-- Header -->
    <header id="coworking-header" class="header-wrapper home-parallax home-fade random-header" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url({{ URL::asset('/storage/assets/'.$coworking->id_user.'/'.$namaco.'/header/'.$coworking->header_co) }}) no-repeat center;background-size: cover; background-attachment: fixed;">
        <div class="header-overlay"></div>
            <div class="header-wrapper-inner">
            <div class="container">
                <div class="welcome-speech">
                    <h5>List Daftar Pemesanan</h5>
                    <a href="{{ url('coworking/profile/'.$coworking->id_co) }}"><p class="text-uppercase"><u>{{ $coworking->nama_co }}</u></p></a>                    
                </div><!-- /.intro -->
            </div>
        </div>
    </header>

    <!-- Table Daftar Pemesanan User -->
    <div class="booking">
        @include('partials.messages')
        <div class="container">
            <table class="table table-bordered table-hover table-responsive">
                <thead class="text-center thead-dark">  
                    <th>Atas Nama</th>
                    <th>Tanggal Sewa</th>
                    <th>Jenis Ruang</th>
                    <th>Jenis Waktu</th>
                    <th>Lama Sewa</th>
                    <th>Total Bayar</th>
                    <th>Status Pemesanan</th>
                    <th>Status Bayar</th>
                    <th>Detail Pilihan</th>
                </thead>
                <tbody>
                    @foreach($datas as $data)
                        <tr>
                            <td>
                                <a href="{{ url('profile/'.$data->id_user) }}" role="button">{{ $data->nama }}</a>
                            </td>
                            <td>{{ $data->tgl_sewa }}</td>
                            <td>{{ $data->jenis_ruang }}</td>
                            <td>{{ $data->jenis_waktu }}</td>
                            <td>{{ $data->waktu_sewa }}</td>
                            <td>{{ $data->total_bayar }}</td>
                            <td>{{ $data->status_booking }}</td>
                            <td>{{ $data->status_bayar }}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#modal{{$data->id_booking}}" class="btn btn-primary" role="button">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $datas->links() }}
        </div>
    </div>

    <!-- Modal Detail -->   
    @foreach($datas as $data)     
        <div class="modal fade" id="modal{{$data->id_booking}}" tabindex="-1" role="dialog" aria-labelledby="modal{{$data->id_booking}}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Modal-label-1">Detail Pemesanan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><i class="fa fa-times"></i></span></button>
                    </div>
                    <form action="{{ url('coworking/booking/'.$data->id_booking)}}" method="POST" enctype="multipart/form-data" file="true">
                        <input type="hidden" name="_method" value="put">
                        @csrf
                        <div class="modal-body">
                            <div class="container">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Detail Pemesanan <u class="text-uppercase">{{ $data->booknama }}</u>. Dengan nomor pemesanan: <u>{{ $data->id_booking }}</u></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td colspan="2"><i>Data Pribadi Penyewa</i></td></tr>
                                        <tr>
                                            <td>Nomor ID FimeSpace</td>
                                            <td>{{ $data->id }}</td>
                                        </tr>
                                        <tr>
                                            <td>Atas Nama</td>
                                            <td class="text-uppercase">{{ $data->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>{{ $data->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>Telepon</td>
                                            <td>{{ $data->telp }}</td>
                                        </tr>
                                        <tr><td colspan="2"><i>Data Pemesanan</i></td></tr>
                                        <tr>
                                            <td>Nomor Pemesanan</td>
                                            <td>{{ $data->id_booking }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pemesanan</td>
                                            <td>{{ $data->created_at }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Coworking</td>
                                            <td>{{ $data->nama_co }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Sewa</td>
                                            <td>{{ $data->tgl_sewa }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Ruangan</td>
                                            <td>{{ $data->jenis_ruang }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Waktu Sewa</td>
                                            <td>{{ $data->jenis_waktu }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lama Sewa</td>
                                            <td>{{ $data->waktu_sewa }}</td>
                                        </tr>
                                        <tr>
                                            <td>Banyak Orang</td>
                                            <td>{{ $data->banyak_orang }}</td>
                                        </tr>
                                        <tr>
                                            <td>Catatan Pemesanan</td>
                                            <td>{!! $data->pesan !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Bayar</td>
                                            <td>{{ $data->total_bayar }}</td>
                                        </tr> 
                                        <tr class="hiddenForm">
                                            <td>id_co</td>
                                            <td>
                                                <div class="form-group row">
                                                    <label for="id_co" class="col-md-3 col-form-label">Id Coworking</label>
                                                    <div class="col-md-9">
                                                        <input id="id_co" type="text" class="form-control{{ $errors->has('id_co') ? ' is-invalid' : '' }}" value="{{ $data->id_co }}" name="id_co" required>
                                                        @if ($errors->has('id_co'))
                                                            <span class="invalid-feedback">
                                                                <strong>{{ $errors->first('id_co') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status Bayar</td>
                                            <td>                                           
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group mb-3">
                                                            <select class="custom-select" id="status_bayar" name="status_bayar">
                                                                <option selected>{{ $data->status_bayar }}</option>
                                                                <option value="Belum">Belum</option>
                                                                <option value="Lunas">Lunas</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>                                 
                                        <tr>
                                            <td>Status Pemesanan</td>
                                            <td>                                           
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group mb-3"> 
                                                            <select class="custom-select" id="status_booking" name="status_booking">
                                                                <option selected>{{ $data->status_booking }}</option>
                                                                <option value="Proses">Proses</option>
                                                                <option value="Selesai">Selesai</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Testimoni</td>
                                            <td>
                                                @if($data->testimoni == Null)
                                                    Belum ada ulasan
                                                @else
                                                    {{ $data->testimoni }}
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary pull-right" role="button">Simpan Data</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('footer')
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@endsection