    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
          @guest
          <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}">
              {{ config('app.name', 'Laravel') }}
          </a>
          @else
          <a class="navbar-brand js-scroll-trigger" href="{{ url('/dashboard') }}">
              {{ config('app.name', 'Laravel') }}
          </a>
          @endguest
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
              @guest
              <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#whoami">Apa Itu FimeSpace?</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#team">List</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#services">Coworkers</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link js-scroll-trigger" href="#about">Coworking</a>
              </li>
          @else
              <li class="nav-item">
                  <a class="nav-link" href="{{ url('profile/'.Auth::user()->id) }}">
                      Profil
                  </a>
              </li>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        {{-- Ubah Profil --}}
                        <a class="dropdown-item px-2" href="{{ url('profile/setting/'.Auth::user()->id) }}">
                            <span><i class="fa fa-wrench icondropdown"></i></span> Pengaturan Akun
                        </a>
                        {{-- Keluar --}}
                        <a class="dropdown-item px-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <span><i class="fa fa-sign-out icondropdown"></i></span> Keluar
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                  </div>
              </li>
          @endguest
          </ul>
        </div>
      </div>
    </nav>