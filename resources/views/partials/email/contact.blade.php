@extends('layouts.master')
@section('title')
	Pesan Untuk Coworking Space
@endsection

@section('header')
    <style>
        hr{
            margin:2% 0;
        }
        h3{
            font-weight: 800;
            line-height: 20px;
            letter-spacing: 0.2px;
        }
        p{
            line-height: 20px;
            letter-spacing: 0.2px;
            font-size: 12px;
            color: #000;
        }
        img{
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        .marginBottom{
            margin-bottom: 30px;
        }
        .pesan{
            width: 70%;
            margin-left:auto;
            margin-right:auto;
        }
    </style>
@endsection

@section('content')
   <div class="pesan marginBottom">
        <img style="width:40%;" src="{{ $message->embed(public_path() . '/assets/logo/logo-dark.png') }}" alt="Logo Fimespace" />
        <h3>Hallo, Coworking space anda mendapatkan pesan baru dari: <u>{{ $nama }}</u></h3>
        <hr/>
        <p>Pesan tersebut adalah: </p>
        <p class="marginBottom">" {{ $pesan }} "</p>
        <p>Dikirim dengan email: {{ $pengirim }} </p>
        <p>Dengan nomor telepon: {{ $telp }}</p>
        <hr/>
        <p><i>Catatan:</i></p>
        <p class="marginBottom"><i>Silahkan anda membalas pesan ini ke email <u>{{ $pengirim }} </u></i></p>
        <div>
            <p class="marginBottom">Terimakasih,</p>
            <p><b>FimeSpace</b></p>
        </div>
   </div>
@endsection