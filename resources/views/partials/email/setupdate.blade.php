@extends('layouts.master')
@section('title')
	Verifikasi Email Kamu
@endsection

@section('header')
    <style>
        h1{
            text-align: center;
            margin-bottom: 40px;
        }
        p{
            text-align: center;
            margin-bottom: 40px;
            line-height: 23px;
            letter-spacing: 0.2px;
            font-size: 16px;
            font-weight: 500;
            word-wrap: break-word;
            color: #515769;
        }
        img,a {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        .marginBottom{
            margin-bottom: 40px;
        }
        .email{
            width: 70%;
            margin-left:auto;
            margin-right:auto;
        }
        .btn {
            text-align: center;
            font-family: Montserrat, 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            font-size: 18px;
        }
        .btn-primary {
            width: auto;
            padding: 10px;
            color: #fff;
            border: 1px solid #aeaeae;
            background-color: #515769;
            border-color: #515769;
        }
    </style>
@endsection

@section('content')
   <div class="email marginBottom">
        <img style="width:60%;" src="{{ $message->embed(public_path() . '/assets/logo/logo-dark.png') }}" alt="Logo Fimespace" />
        <h1>Notifikasi Perubahan Akun</h1>
        <hr/>
        <p>Akun anda telah diperbaharui. Terjadi perubahan pada {{ $setupdate }}. Deskripsi perubahan adalah: </p>
        <hr/>
        @if($setupdate == 'email')            
            <p>Email lama anda adalah <u>{{ $tujuan }}</u> menjadi <b>{{ $databaru }}</b> </p>
        @endif
        @if($setupdate == 'password')   
            Jika anda tidak merubah password, silahkan klik link dibawah ini.         
            <a class="btn btn-primary" href="{{ route('password.request') }}"  style="width: 50%;">
                Reset Password
            </a>
        @endif
    <hr>
    <h2 class="marginBottom">Terima Kasih,</h2>
    <h3>Fimespace</h3>
   </div>
@endsection