@extends('layouts.master')
@section('title')
	Verifikasi Email Kamu
@endsection

@section('header')
    <style>
        h1{
            text-align: center;
            margin-bottom: 40px;
        }
        p{
            text-align: center;
            margin-bottom: 40px;
            line-height: 23px;
            letter-spacing: 0.2px;
            font-size: 16px;
            font-weight: 500;
            word-wrap: break-word;
            color: #515769;
        }
        img,a {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        .marginBottom{
            margin-bottom: 40px;
        }
        .email{
            width: 70%;
            margin-left:auto;
            margin-right:auto;
        }
        .btn {
            text-align: center;
            font-family: Montserrat, 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            font-size: 18px;
        }
        .btn-primary {
            width: auto;
            padding: 10px;
            color: #fff;
            border: 1px solid #aeaeae;
            background-color: #515769;
            border-color: #515769;
        }
    </style>
@endsection

@section('content')
   <div class="email marginBottom">
        <img style="width:60%;" src="{{ $message->embed(public_path() . '/assets/logo/logo-dark.png') }}" alt="Logo Fimespace" />
        <h1>Akun Konfirmasi Registerasi</h1>
        <hr/>
        <p>Terimakasih telah membuat akun untuk mengakses website Fimespace. Untuk dapat menggunakan fitur-fitur yang ada didalam Fimespace, kamu harus menverifikasikan akun kamu terlebih dahulu: </p>
        <br/>
        <a class="btn btn-primary" href="{{ route('sendEmailDone', ["email" => $user->email, "verifyToken" => $user->verifyToken]) }}" style="width: 50%;">Klik disini</a>
   </div>
@endsection