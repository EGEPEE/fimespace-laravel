@extends('layouts.master')

@section('title')
	Dashboard
@endsection

@section('header')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- Header -->
    <header id="search-header" class="header-wrapper home-parallax home-fade random-header" style="background-image: url( {{ asset('assets/bg-dashboard.jpg')}});">
        <div class="header-overlay"></div>
        <div class="header-wrapper-inner">
            <div class="container">
                <div class="welcome-speech">
                    <h5>ERROR: 404 NOT FOUND</h5>
                </div>
            </div>
        </div>
    </header>

    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="section-title text-center">
                    <h2>{{ $message }}</h2>
                    <br>
                    <a class="btn btn-primary-second btn-daftar" role="button" href="{{ url('/') }}"><b>Kembali Ke Beranda</b></a>
                </div>
            </div>
        </div><!-- end container -->
    </section>

@endsection

@section('footer')
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCyAGNSrdEm76jTm6T-BVmGcsfBgIhJRq4&libraries=places&callback=geoLocationInit"></script>
 
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('jquery/jquery1.11.0-ui.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('js/agency.min.js') }}"></script>
@endsection