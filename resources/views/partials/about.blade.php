<!-- About -->
<section id="about">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading text-uppercase">Cara Kerja Coworking</h2>
        <h3 class="section-subheading text-muted">Mencari coworkers yang mudah dan aman.</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <ul class="timeline">
          <li>
            <div class="timeline-image">
              <img class="rounded-circle img-fluid" src="{{ asset('assets/3.jpg') }}" alt="">
            </div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h4>Tahap I</h4>
                <h4 class="subheading">Pasang Coworking Space</h4>
              </div>
              <div class="timeline-body">
                <p class="text-muted">Mendeskripsikan coworking space-mu mulai dari suasana tempat, lokasi dan fasilitas-fasilitas yang disediakan, tambahkan foto-foto agar calon coworkers dapat merasakan sensasi yang terdapat dalam coworking spacemu.</p>
              </div>
            </div>
          </li>
          <li class="timeline-inverted">
            <div class="timeline-image">
              <img class="rounded-circle img-fluid" src="{{ asset('assets/2.jpg') }}" alt="">
            </div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h4>Tahap II</h4>
                <h4 class="subheading">Informasi Jalur komunikasi</h4>
              </div>
              <div class="timeline-body">
                <p class="text-muted">Sediakan informasi mengenai kontak dari coworking spacemu untuk dapat mulai mengirimkan pesan, berdiskusi one on one dengan pihak administrasi coworking space.</p>
              </div>
            </div>
          </li>
          <li>
            <div class="timeline-image">
              <img class="rounded-circle img-fluid" src="{{ asset('assets/4.jpg') }}" alt="">
            </div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h4>Tahap III</h4>
                <h4 class="subheading">Negoisasi Harga dan Jadwal Booking</h4>
              </div>
              <div class="timeline-body">
                <p class="text-muted">Komunikasikan mengenai jadwal booking yang ada dicoworking space serta coworkers dapat menggali informasi lain mengenai coworking space. Setelah itu, jika setuju maka coworkers dapat menggunakan coworking sesuai ketentuan tiap coworking.</p>
              </div>
            </div>
          </li>
          <li class="timeline-inverted">
            <div class="timeline-image">
              <h4>Gabung
                <br>Coworking Space!</h4>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>