@extends('layouts.master')

@section('script2')
<script>
    $(document).ready(function(){
        //=====================================================================================
        //    ADDCO AND EDIT CO
        //=====================================================================================*/
        $("#informasi,#komunikasi,#lokasi,#fasilitas,#foto,#pembayaran,#harga,#simpan").hide();
        $(".service a").click(function(){
            hrefservice = $(this).attr('href');
            console.log(hrefservice);
    
            $('.form-content-choose').each(function(){
                var ids = '#'+$(this).attr('id');
                if(ids != hrefservice){
                    $(this).hide();
                }else{
                    $(this).show();
                    $("#opening").hide();
                    $(this).removeClass("hidden");
                }
            });
    
        });
        //=====================================================================================
        //    COUNTER ON HOME
        //=====================================================================================*/    
        $('.count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(window).scroll(function() {
            if ($(".navbar").offset().top > 50) {
                $(".navbar-fixed-top").addClass("top-nav-collapse");
            } else {
                $(".navbar-fixed-top").removeClass("top-nav-collapse");
            }
        });
        //=====================================================================================
        //        INPUT
        //=====================================================================================*/
        $('.custom-file-input').on('change', function() { 
            let fileName = $(this).val().split('\\').pop(); 
            $(this).next('.custom-file-label').addClass("selected").html(fileName); 
        });
        //=====================================================================================
        //          HARGA SEWA UL LI
        //=====================================================================================*/
        $("#harga-sewa ul").addClass("list-unstyled pricing-list margin-b-50");
        $("#harga-sewa ul li").addClass("pricing-list-item");
        
        //=====================================================================================
        //          HARGA FASILITAS
        //=====================================================================================*/
        $("#fasilitasco ul").addClass("list-unstyled pricing-list margin-b-50");
        $("#fasilitasco ul li").addClass("pricing-list-item");

        CKEDITOR.replace("catatan_coworking");
        CKEDITOR.replace("article-ckeditor");
        CKEDITOR.replace("fasilitas_event");
    
        $('#datepicker').datepicker({
            changeMonth: true,
            changeYear: true
        });
    
        $('.alert').alert();

        //=====================================================================================
        //    NAVBAR
        //=====================================================================================*/   
        $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });
</script>
@endsection