<!-- Header -->
<header id="header" class="header-wrapper home-parallax home-fade" style="background-image: {{ $img }}">
    <div class="header-overlay"></div>
    <div class="header-wrapper-inner">
        <div class="container">

            <div class="welcome-speech">
                <h1>{{ $title }}</h1>
                <p class="text-uppercase">{{ $caption }}</p>
            </div><!-- /.intro -->
            
        </div><!-- /.container -->

    </div><!-- /.header-wrapper-inner -->
</header>