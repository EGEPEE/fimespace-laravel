@extends('layouts.master')

@section('title')
    Login
@endsection

@section('header')
    <!-- THEME STYLES -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <!-- Navbar -->
    @include('partials.navbar.navbar2')
    <!-- Content -->
    <div class="bgLogin">
        <section class="login container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="authform shadow">
                        <h2 class="text-center">Login</h2>
                        <img src="{{ asset('assets/logo/logo-bg-dark.png') }}" alt="Logo Fimespace">
                        @include('partials.messages')
                        <div class="container">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right">Email</label>
    
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Masukkan Alamat Email" value="{{ old('email') }}" required autofocus>
    
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
    
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
    
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Masukkan Password" name="password" required>
    
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
    
                                {{-- <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="checkbox">
                                            <label>
                                                <input class type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Ingatkan Saya
                                            </label>
                                        </div>
                                    </div>
                                </div> --}}
    
                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-daftar">
                                            Login
                                        </button><br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    Lupa Password?
                                                </a>
                                            </div>
                                            <div class="col-sm-6">
                                                <a class="btn btn-link" href="{{ route('register') }}">
                                                    Buat Akun
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('footer')
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@endsection