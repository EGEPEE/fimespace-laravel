@extends('layouts.master')

@section('title')
    Register
@endsection

@section('header')
    <!-- THEME STYLES -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <!-- Navbar -->
    @include('partials.navbar.navbar2')
    <!-- Content -->
    <div class="bgScreen">
        <div class="container">
            <div class="content">
                <div class="row">
                    <div class="col-md-5 marginBottom">
                        <div class="card shadow">
                            <div class="card-body  text-center">
                                <h2>Buat Akun</h2>
                                <hr>
                                <div class="row masuk">
                                    <div class="col-sm-6 col-md-12">
                                        <h6>Sudah punya akun di Fimespace?</h6>
                                    </div>
                                    <div class="col-sm-6 col-md-12">
                                        <h6>
                                            <a class="nav-link" href="{{ route('login') }}">Masuk</a>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="authform shadow">
                            <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Masukkan Nama Lengkap" value="{{ old('name') }}" required autofocus>
                    
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>                                         
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Masukkan Alamat Email" name="email" value="{{ old('email') }}" required>
                    
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                    
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Masukkan Password" name="password" required>
                    
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                    
                                    <div class="form-group marginBottom">
                                        <label for="password-confirm">Konfirmasi Password</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Masukkan Password Yang Sama" required>
                                    </div>
                    
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary-second">
                                            Daftar
                                        </button>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@endsection
