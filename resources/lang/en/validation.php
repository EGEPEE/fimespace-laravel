<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => ':attribute URL tidak valid.',
    'after'                => ':attribute harus berupa a date after :date.',
    'after_or_equal'       => ':attribute harus berupa a date after or equal to :date.',
    'alpha'                => ':attribute hanya boleh berisikan huruf.',
    'alpha_dash'           => ':attribute hanya boleh berisikan huruf, angka, dan dashes.',
    'alpha_num'            => ':attribute hanya boleh berisikan huruf dan angka.',
    'array'                => ':attribute harus berupa an array.',
    'before'               => ':attribute harus berupa a date before :date.',
    'before_or_equal'      => ':attribute harus berupa a date before or equal to :date.',
    'between'              => [
        'numeric' => ':attribute harus diantara :min dan :max.',
        'file'    => ':attribute harus diantara :min dan :max kilobit.',
        'string'  => ':attribute harus diantara :min dan :max karakter.',
        'array'   => ':attribute harus diantara :min dan :max item.',
    ],
    'boolean'              => ':attribute field harus berisika.',
    'confirmed'            => ':attribute konfirmasi tidak cocok.',
    'date'                 => ':attribute is not a valid date.',
    'date_format'          => ':attribute does not match the format :format.',
    'different'            => ':attribute and :other harus berupa different.',
    'digits'               => ':attribute harus berupa :digits digits.',
    'digits_between'       => ':attribute harus berupa between :min and :max digits.',
    'dimensions'           => ':attribute has invalid image dimensions.',
    'distinct'             => ':attribute field has a duplicate value.',
    'email'                => ':attribute harus berupa a valid email address.',
    'exists'               => ':attribute yang telah dipilih tidak valid.',
    'file'                 => ':attribute harus berupa a file.',
    'filled'               => ':attribute field harus diisi.',
    'image'                => ':attribute harus berupa gambar.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => ':attribute tidak boleh lebih dari :max.',
        'file'    => ':attribute tidak boleh lebih dari :max kilobytes.',
        'string'  => ':attribute tidak boleh lebih dari :max karakter.',
        'array'   => ':attribute tidak boleh lebih dari :max items.',
    ],
    'mimes'                => ':attribute harus berupa a file of type: :values.',
    'mimetypes'            => ':attribute harus berupa a file of type: :values.',
    'min'                  => [
        'numeric' => ':attribute tidak boleh kurang dari :min.',
        'file'    => ':attribute tidak boleh kurang dari :min kilobytes.',
        'string'  => ':attribute tidak boleh kurang dari :min characters.',
        'array'   => ':attribute tidak boleh kurang dari :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => ':attribute harus berupa angka.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => ':attribute format tidak valid.',
    'required'             => ':attribute field harus diisi.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => ':attribute harus berupa huruf.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => ':attribute telah diambil.',
    'uploaded'             => ':attribute tidak dapat diupload.',
    'url'                  => ':attribute format tidak valid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
