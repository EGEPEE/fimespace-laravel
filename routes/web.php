<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// Home user
Route::get('/', 'HomeController@index');

// Dashboard
Route::get('dashboard', 'DashboardController@index');
Route::get('dashboard/search', 'DashboardController@search');

// Profile User
Route::resource('profile', 'ProfileController');
Route::get('profile/setting/{profile}', 'ProfileController@setting');
Route::put('profile/setting-update/{profile}', 'ProfileController@settingupdate');


// Coworking of User
Route::resource('coworking/profile', 'CoworkingController');
Route::post('coworking/profile/ulasan', 'CoworkingController@ulasan');
Route::post('coworking/profile/contact/{id}', 'CoworkingController@contact');
Route::delete('coworking/photo/{idco}/{idfo}', 'CoworkingController@deletephoto');

// Booking
Route::resource('coworking/booking', 'BookingController');

// Booking User
Route::get('profile/booking/{id}', 'ProfileController@booking');
Route::get('verifyEmailFirst', 'Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');
Route::get('verify/{email}/{verifyToken}','Auth\RegisterController@sendEmailDone')->name('sendEmailDone');

// clear-cache
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
