<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coworkings extends Model
{
    // Table name
    protected $table = 'coworkings';
    // Primary Key
    protected $primaryKey = 'id_co';
    public $timestamps = true;
}
