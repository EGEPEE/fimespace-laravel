<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usermodels extends Model
{
    // Table name
    protected $table = 'users';
    // Primary Key
    protected $primaryKey = 'id';
    public $timestamps = true;
}
