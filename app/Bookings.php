<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    // Table name
    protected $table = 'bookings';
    // Primary Key
    protected $primaryKey = 'id_booking';
    public $timestamps = true;
}
