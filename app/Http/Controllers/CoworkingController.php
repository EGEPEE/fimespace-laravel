<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Photos;
use App\Usermodels;
use App\Coworkings;
use App\Bookings;
use DB;
use Mail;
use Image;
use Storage;


class CoworkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $datas = Coworkings::orderBy('created_at', 'DESC')->get();
        $search = null;
        
        return view('pages/dashboard', compact('datas', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/coworking/addco');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_coworking' => 'required|string|max:32', 
            'desc_coworking'  => 'required|string|min:20',
            'nama_utama_coworking' => 'required|string|max:32',
            'bank_utama_coworking' => 'required|string|max:10',
            'rek_utama_coworking' => 'required|string|max:16',
            'nama_kedua_coworking' => 'nullable|string|max:32',            
            'bank_kedua_coworking' => 'nullable|string|max:10',
            'rek_kedua_coworking' => 'nullable|string|max:16',
            'kota_coworking' => 'required',
            'alamat_coworking' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'email_coworking' => 'required|string|max:32',
            'telp_coworking' => 'required|string|max:12',
            'website_coworking'  => 'nullable|string|max:100',
            'fb_coworking' => 'nullable|string|max:100',
            'twitter_coworking' => 'nullable|string|max:100',
            'ig_coworking' => 'nullable|string|max:100',
            'fasilitas_coworking' => 'required|min:10|max:450',
            'logo_coworking' => 'image|required|max:1500',
            'header_coworking' => 'image|required|max:1500',
            'file' => 'image|nullable|max:1500',
            'harga_co_desc' => 'nullable|max:255',
            'harga_perhari' => 'required|integer',
            'harga_perbulan' => 'nullable|integer',
            'harga_pertahun' => 'nullable|integer',
            'harga_meeting_desc' => 'nullable|min:10|max:255',
            'harga_meeting_perjam' => 'nullable|integer',
            'harga_meeting_perhari' => 'nullable|integer',
            'harga_meeting_perbulan' => 'nullable|integer',
            'harga_event_desc' => 'nullable|min:10|max:255',
            'harga_event' => 'nullable|integer',
            'harga_coworking' => 'nullable|image|max:10240',
            'bayar_ots' => 'required|string|max:5',
            'persetujuan' => 'required',
        ]);

        // New Post
        $coworking = new Coworkings();

        $nama_coworking = $request['nama_coworking'];

        $id_user = auth()->user()->id;

        if($request->hasFile('logo_coworking')){
            $filenametostore = $this->uploadfunc($request->file('logo_coworking'), 'logo', $id_user, $nama_coworking, 500, 150);

            $coworking->logo_co = $filenametostore;
        }
        if($request->hasFile('header_coworking')){
            
            $filenametostore = $this->uploadfunc($request->file('header_coworking'), 'header', $id_user, $nama_coworking);

            $coworking->header_co = $filenametostore;
        }
        $coworking->id_user = auth()->user()->id;
        $coworking->nama_co = $request['nama_coworking'];
        $coworking->desc = $request['desc_coworking'];
        $coworking->kota = $request['kota_coworking'];
        $coworking->alamat_co = $request['alamat_coworking'];
        $coworking->nama_utama = $request['nama_utama_coworking'];
        $coworking->bank_utama = $request['bank_utama_coworking'];
        $coworking->rek_utama = $request['rek_utama_coworking'];
        $coworking->nama_kedua = $request['nama_kedua_coworking'];
        $coworking->bank_kedua = $request['bank_kedua_coworking'];
        $coworking->rek_kedua = $request['rek_kedua_coworking'];
        $coworking->lat = $request['lat'];
        $coworking->lng = $request['lng'];
        $coworking->email_co = $request['email_coworking'];
        $coworking->telp = $request['telp_coworking'];
        $coworking->website = $request['website_coworking'];
        $coworking->fb = $request['fb_coworking'];
        $coworking->twitter = $request['twitter_coworking'];
        $coworking->ig = $request['ig_coworking'];
        $coworking->fasilitas = $request['fasilitas_coworking'];
        $coworking->coworking_desc = $request['harga_co_desc'];
        $coworking->coworking_perhari = $request['harga_perhari'];
        $coworking->coworking_perbulan = $request['harga_perbulan'];
        $coworking->coworking_pertahun = $request['harga_pertahun'];
        $coworking->meeting_desc = $request['harga_meeting_desc'];
        $coworking->meeting_perjam = $request['harga_meeting_perjam'];
        $coworking->meeting_perhari = $request['harga_meeting_perhari'];
        $coworking->meeting_perbulan = $request['harga_meeting_perbulan'];
        $coworking->event_desc = $request['harga_event_desc'];
        $coworking->event_harga = $request['harga_event'];
        $coworking->bayar_ots = $request['bayar_ots'];
        $coworking->persetujuan = 'Setuju';        

        $coworking->save();

        //Photo
        if($request->hasFile('file')){
            foreach ($request->file as $file){            
                $filenametostore = $this->uploadfunc($file, 'foto', $id_user, $nama_coworking);
                
                $photo = new Photos();
                $photo->id_co = $coworking->id_co;
                $photo->foto_co = $filenametostore;
                $photo->save();
            }
        }

        return redirect()->to('/dashboard')->with('success', 'Profil Coworking Space Telah Dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coworking = Coworkings::find($id);
        $datas = Bookings::select('*')
                ->join('Coworkings', 'Coworkings.id_co', '=', 'Bookings.id_co')
                ->join('Users', 'Users.id', '=', 'Bookings.id_user')
                ->where('Bookings.id_co', $id)->get();
        $dataulasan = Bookings::where([
            ['id_co', '=', $id], ['testimoni', '!=', 'null']
        ])->paginate(3);
        $photos = Photos::where('id_co', '=', $id)->paginate(6);

        // $search = null;

        return view('pages/coworking/profileco', compact('coworking', 'datas', 'dataulasan', 'photos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coworking = Coworkings::find($id);

        if(auth()->user()->id !== $coworking->id_user){
            return view('pages/coworking/profileco')->with('error', 'Anda Tidak Mendapat Akses Mengubah');
        }
        return view('pages/coworking/editco')->with('coworking', $coworking);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'logo_coworking' => 'image|nullable|max:1500',
            'header_coworking' => 'image|nullable|max:1500',
            'file' => 'image|nullable|max:1500',
        ]);
        $coworking = Coworkings::find($id);
        
        $nama_coworking = $request['nama_coworking'];
        $id_user = auth()->user()->id;

        if($request->input('nama_coworking') != $coworking->nama_co){
            Storage::move('public/assets/'.$coworking->id_user.'/'.$coworking->nama_co, 'public/assets/'.$coworking->id_user.'/'.$request['nama_coworking']);
        }
        // Handle file upload
        if($request->hasFile('logo_coworking')){
            $filenametostore = $this->uploadfunc($request->file('logo_coworking'), 'logo', $id_user, $nama_coworking, 500, 150);

            $coworking->logo_co = $filenametostore;
        }
        if($request->hasFile('header_coworking')){

            $filenametostore = $this->uploadfunc($request->file('header_coworking'), 'header', $id_user, $nama_coworking, 500, 350);

            $coworking->header_co = $filenametostore;
        }

        $coworking->id_user = auth()->user()->id;
        $coworking->nama_co = $request['nama_coworking'];
        $coworking->desc = $request['desc_coworking'];
        $coworking->kota = $request['kota_coworking'];
        $coworking->alamat_co = $request['alamat_coworking'];
        $coworking->nama_utama = $request['nama_utama_coworking'];
        $coworking->bank_utama = $request['bank_utama_coworking'];
        $coworking->rek_utama = $request['rek_utama_coworking'];
        $coworking->nama_kedua = $request['nama_kedua_coworking'];
        $coworking->bank_kedua = $request['bank_kedua_coworking'];
        $coworking->rek_kedua = $request['rek_kedua_coworking'];
        $coworking->lat = $request['lat'];
        $coworking->lng = $request['lng'];
        $coworking->email_co = $request['email_coworking'];
        $coworking->telp = $request['telp_coworking'];
        $coworking->website = $request['website_coworking'];
        $coworking->fb = $request['fb_coworking'];
        $coworking->twitter = $request['twitter_coworking'];
        $coworking->ig = $request['ig_coworking'];
        $coworking->fasilitas = $request['fasilitas_coworking'];
        $coworking->coworking_desc = $request['harga_co_desc'];
        $coworking->coworking_perhari = $request['harga_perhari'];
        $coworking->coworking_perbulan = $request['harga_perbulan'];
        $coworking->coworking_pertahun = $request['harga_pertahun'];
        $coworking->meeting_desc = $request['harga_meeting_desc'];
        $coworking->meeting_perjam = $request['harga_meeting_perjam'];
        $coworking->meeting_perhari = $request['harga_meeting_perhari'];
        $coworking->meeting_perbulan = $request['harga_meeting_perbulan'];
        $coworking->event_desc = $request['harga_event_desc'];
        $coworking->event_harga = $request['harga_event'];
        $coworking->bayar_ots = $request['bayar_ots'];

        $coworking->save();

        //Photo
        if($request->hasFile('file')){
            foreach ($request->file as $file){            
                $filenametostore = $this->uploadfunc($file, 'foto', $id_user, $nama_coworking, 500, 500);
                
                $photo = new Photos();
                $photo->id_co = $coworking->id_co;
                $photo->foto_co = $filenametostore;
                $photo->save();
            }
        }
        
        return redirect()->to('coworking/profile/'.$id)->with('success', 'Profil Coworking Space Telah Diperbaharui.');
    }
    public function uploadfunc($reqfile, $foldermain, $id_user, $nama_coworking, $h, $w){
        //get filename with extension
        $filenamewithextension = $reqfile->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $reqfile->getClientOriginalExtension();

        //filename to store
        $filenametostore = $filename.'_'.time().'.'.$extension;

        //Upload File
        $reqfile->storeAs('public/assets/'.$id_user.'/'.$nama_coworking.'/'.$foldermain, $filenametostore);

        //Resize image here
        $thumbnailpath = 'storage/assets/'.$id_user.'/'.$nama_coworking.'/'.$foldermain.'/'.$filenametostore;
        $img = Image::make($thumbnailpath)->resize($h, $w, function($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($thumbnailpath);

        return $filenametostore;
}


    public function contact(Request $request, $id){
        $coworking = Coworkings::find($id);

        $request->validate([
            'kontak_nama' => 'required',
            'kontak_email' => 'required|email',
            'kontak_subjek' => 'min:3',
            'kontak_telp' => 'max:12',
            'kontak_pesan' => 'min:10',
            ]);
        
        $data = array(
            'nama' => $request['kontak_nama'],    
            'tujuan' => $coworking->email_co,
            'pengirim' => $request['kontak_email'],
            'subjek' => $request['kontak_subjek'],
            'telp' => $request['kontak_telp'],    
            'pesan' => $request['kontak_pesan'],
        );

        Mail::send('partials.email.contact', $data, function($message) use($data){
            $message->from($data['pengirim']);
            $message->to($data['tujuan']);
            $message->subject($data['subjek']);
        });

        return redirect()->to('coworking/profile/'.$id)->with('success', 'Pesan anda telah terkirim, silahkan menunggu respon di email anda.');
    }

    public function deletephoto($id){
        $photo = Photos::select('*')
                ->join('Coworkings', 'Coworkings.id_co', '=', 'Cophoto.id_co')
                ->where('Cophoto.id_foto', $id)->get();
        Storage::deleteDirectory('public/assets/'.$photo->id_user.'/'.$photo->nama_co.'/foto_coworking/'.$photo->foto_co);

        return 'hapus foto';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coworking = Coworkings::find($id);
        Storage::deleteDirectory('public/assets/'.$coworking->id_user.'/'.$coworking->nama_co);
        $coworking->delete();

        return redirect()->to('/dashboard')->with('Sukses', 'Coworking Anda Telah dihapus.');
    }
}
