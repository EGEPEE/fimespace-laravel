<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coworkings;
use App\User;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct(){
    //     $this->middleware('auth');
    // }
     
    public function index()
    {
        $search = null;
        $kota = '0';
        $coworkings = Coworkings::all();
        $coworkinglist = Coworkings::orderBy('created_at', 'DESC')->get();
        $User = User::all();
        $kota = Coworkings::distinct('kota')->count('kota');
        $search = null;
        
        return view('pages/home', compact('coworkings', 'coworkinglist', 'users', 'search', 'kota'));
    }

    public function search(Request $request)
    {   
        $search = $request['search_co'];
        $datas = Coworkings::where('nama','LIKE', '%'.$search.'%')->orWhere('kota','LIKE', '%'.$search.'%')->orWhere('alamat_co','LIKE', '%'.$search.'%')->paginate(10);
        
        return view('pages/dashboard', compact('datas', 'search'));
    }
}
