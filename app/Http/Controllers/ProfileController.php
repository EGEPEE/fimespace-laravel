<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\Coworkings;
use App\Bookings;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
     }
     
    public function index()
    {
        return view('pages/profile/profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    //Booking Coworking From User
    public function booking($id)
    {
        $datas = Bookings::select('*')
                ->join('coworkings', 'coworkings.id_co', '=', 'bookings.id_co')
                ->where('bookings.id_user', $id)->paginate(20);
        $user = User::find($id);

        return view('pages/profile/booking', compact('user', 'datas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $data = Coworkings::where('id_user', '=', $id)->get();

        return view('pages/profile/profile', compact('user', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $data = Coworkings::where('id_user', '=', $id)->get();
        $coworking = Coworkings::orderBy('id_user', 'ASC')->get();

        return view('pages/profile/uprofile', compact('user', 'coworking', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'foto_profil' => 'image|nullable|max:1000',
        ]);
        $usermodel = User::find($id);

        if($request->hasFile('foto_profil')){
            //get file name with extension
            $filenameWithExt = $request->file('foto_profil')->getClientOriginalName();
            //get just filename
            $fileName = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just ext
            $extension = $request->file('foto_profil')->getClientOriginalExtension();
            //filename to store
            $fileNameToStoreProfile = $fileName.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('foto_profil')->storeAs('public/assets/'.$id.'/foto_profil', $fileNameToStoreProfile);
        
            $usermodel->foto_profil = $fileNameToStoreProfile;
        }

        $usermodel->name = $request['name'];
        $usermodel->jk = $request['jk'];
        $usermodel->no_telp = $request['no_telp'];
        $usermodel->alamat = $request['alamat'];

        $usermodel->save();
        
        return redirect()->to('profile/'.$id)->with('Sukses', 'Profil Telah Diperbaharui.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = User::find($id);
        $delete->delete();
        Storage::deleteDirectory('public/assets/'.$id);

        return redirect()->to('/')->with('Sukses', 'Akun anda telah dihapus.');
    }

    // Setting
    public function setting($id)
    {
        $user = User::find($id);
        $data = Coworkings::where('id_user', '=', $id)->get();
        $coworking = Coworkings::orderBy('id_user', 'ASC')->get();

        return view('pages/profile/setting', compact('user', 'coworking', 'data'));
    }

    public function settingupdate(Request $request, $id)
    {
        $usermodel = User::find($id);
        $query = User::select('email', 'password')->where('id', $id)->first();

        if(($request['email'] != $query->email) && ($request['pass_baru'] == null)){
            $this->perubahan($query->email, "email", $request['email']);
            $usermodel->email = $request['email'];
            $usermodel->password = $query->password;
        }elseif($request['pass_baru'] != null){
            if (Hash::check($request['pass_lama'], $query->password)) {
                $usermodel->password = Hash::make($request['pass_baru']);
                $usermodel->save();
                return redirect()->back()->with('success', 'Your password has been updated.');
            } else {
                return redirect()->back()->with('warning', 'The current password you provided could not be verified');
            }

        }else{
            $usermodel->email = $query->email;
            $usermodel->password = $query->password;
        }

        $usermodel->name = $request['name'];

        $usermodel->save();

        return redirect()->to('profile/'.$id)->with('Sukses', 'Akun Telah Diperbaharui.');
    }
    public function perubahan($tujuan, $setupdate, $databaru){
        $data = array(
            'pengirim' => 'fimespace.coworking@gmail.com',
            'subject'=> 'Notifikasi Perubahan Akun',
            'tujuan' => $tujuan,
            'databaru' => $databaru,
            'setupdate' => $setupdate,
        );
        Mail::send('partials.email.setupdate', $data, function($message) use($data){
            $message->from($data['pengirim']);
            $message->to($data['tujuan']);
            $message->subject($data['subject']);
        });
    }
}
