<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coworkings;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $datas = Coworkings::orderBy('created_at', 'DESC')->paginate(10);
        $search = null;
        
        return view('pages/dashboard', compact('datas', 'search', 'coworking'));
    }
    public function search(Request $request)
    {   
        $search = $request['search_co'];
        $datas = Coworkings::where('nama_co','LIKE', '%'.$search.'%')->orWhere('kota','LIKE', '%'.$search.'%')->orWhere('alamat_co','LIKE', '%'.$search.'%')->paginate(10);
        
        return view('pages/dashboard', compact('datas', 'search'));
    }
}
