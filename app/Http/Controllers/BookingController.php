<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\User;
use App\Bookings;
use App\Coworkings;

class BookingController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_co' => 'required|integer',
            'nama' => 'required|string|max:32',
            'email' => 'required|string|max:32',
            'telp' => 'required|string|max:12',
            'jenis_waktu' => 'required|string',
            'waktu_sewa' => 'required|string|max:20',
            'banyak_orang' => 'required|string|max:20',
            'tgl_sewa' => 'required|date',
            'pesan' => 'required|string',
        ]);

        $query = Coworkings::select('coworking_perhari', 'coworking_perbulan', 'coworking_pertahun', 'meeting_perjam', 'meeting_perhari', 'meeting_perbulan', 'event_harga')
        ->where('id_co', $request['id_co'])->first();

        $booking = new Bookings();
        
        $booking->id_user = auth()->user()->id;
        $booking->id_co = $request['id_co'];
        $booking->nama = $request['nama'];
        $booking->email = $request['email'];
        $booking->telp = $request['telp'];
        $booking->jenis_ruang = $request['jenis_ruang'];
        $booking->jenis_waktu = $request['jenis_waktu'];
        $booking->waktu_sewa = $request['waktu_sewa'];
        $booking->banyak_orang = $request['banyak_orang'];
        $booking->tgl_sewa = $request['tgl_sewa'];
        $booking->status_booking = 'Proses';
        $booking->status_bayar= 'Belum';
        $booking->pesan = $request['pesan'];
        
        if($request['jenis_ruang'] == 'Coworking'){
            if($request['jenis_waktu'] == 'Perhari'){
                $data = $query->coworking_perhari;
                $booking->total_bayar = $request['waktu_sewa'] * $data;
            }elseif($request['jenis_waktu'] == 'Perbulan'){
                $data = $query->coworking_perbulan;
                $booking->total_bayar = $request['waktu_sewa'] * $data;
            }else{
                $data = $query->coworking_pertahun;
                $booking->total_bayar = $request['waktu_sewa'] * $data;
            }
        }elseif($request['jenis_ruang'] == 'Meeting' ){
            if($request['jenis_waktu'] == 'Perjam'){
                $data = $query->meeting_perjam;
                $booking->total_bayar = $request['waktu_sewa'] * $data;
            }elseif($request['jenis_waktu'] == 'Perhari'){
                $data = $query->meeting_perhari;
                $booking->total_bayar = $request['waktu_sewa'] * $data;
            }else{
                $data = $query->meeting_perbulan;
                $booking->total_bayar = $request['waktu_sewa'] * $data;
            }
        }elseif($request['jenis_ruang'] == 'Event'){
            $data = $query->event_harga;
            $booking->total_bayar = $request['waktu_sewa'] * $data;
        }else{
            return redirect()->to('/coworking/profile/'.$booking->id_co)->with('error', 'Jenis ruangan tidak ada.'); 
        }

        $booking->save();

        return redirect()->to('/coworking/profile/'.$booking->id_co)->with('success', 'Daftar Booking Anda Telah Terkirim. Silahkan Cek Riwayat Booking Anda Untuk Pembayaran.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datas = Bookings::select('Bookings.email as bookemail', 'Bookings.telp as booktelp', 'Bookings.nama as booknama', 'Coworkings.*', 'Users.*', 'Bookings.*')
                ->join('Coworkings', 'Coworkings.id_co', '=', 'bookings.id_co')
                ->join('Users', 'Users.id', '=', 'bookings.id_user')
                ->where('bookings.id_co', $id)->paginate(20);
        $databooking = Bookings::find($id);
        $coworking = Coworkings::find($id);

        return view('pages/coworking/bookingco', compact('coworking', 'datas', 'databooking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $query = Bookings::where('id_booking', $id)->first();

        $booking = Bookings::find($id);
        if ($request->filled('status_booking')) {
            $booking->status_booking = $request['status_booking'];
            $message = ['success', 'Status Telah Diperbaharui.'];
            $booking->save();
        }
        if ($request->filled('status_bayar')) {
            $booking->status_bayar = $request['status_bayar'];
            $message = ['success', 'Status Telah Diperbaharui.'];            
            $booking->save();
        }
        if ($request->filled('ulasan')) {
            $booking->tgl_testimoni = $request['tgl_testimoni'];
            $booking->testimoni = $request['ulasan'];
            $message = ['success', 'Testimoni Anda Telah Terkirim.'];
            $booking->save();

            return redirect()->to('/profile/booking/'.$query->id_user)->with($message);
        }

        return redirect()->to('/coworking/booking/'.$query->id_co)->with($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
