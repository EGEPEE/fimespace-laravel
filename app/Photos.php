<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{
    // Table name
    protected $table = 'cophoto';
    // Primary Key
    protected $primaryKey = 'id_photo';
    public $timestamps = true;
}
